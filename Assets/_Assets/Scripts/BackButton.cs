﻿using UnityEngine;
using System.Collections;

public class BackButton : MonoBehaviour {

	public string levelName;
	GameObject fadeObject;
	
	void Start(){
		fadeObject = GameObject.Find ("FadeImage");
		
	}

	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			if(levelName == "_Irten_")
			{
				if(Application.loadedLevelName == "menu")
				{
					OptionsButton b = GameObject.Find("Options").GetComponent<OptionsButton>();
					if(b.showing)
					{
						b.IsClicked();
						return;
					}

				}
				Application.Quit();
			}
			else if (levelName == Application.loadedLevelName) 
			{
				Application.LoadLevel (levelName);
			}
			else 
			{
				StartCoroutine(loadFading());
			}
		}
	}

	IEnumerator loadFading(){
		if(fadeObject)
			fadeObject.GetComponent<fadeManager> ().fadeIn ();
		yield return new WaitForSeconds (1f);
		Application.LoadLevel (levelName);
	}
}
