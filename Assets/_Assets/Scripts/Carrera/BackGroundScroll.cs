﻿using UnityEngine;
using System.Collections;

public class BackGroundScroll : MonoBehaviour {


	GameObject player;
	float lastX;
	float lastY;
	float offsetX;
	float offsetY;
	Transform cameraPos;

	ShipMovement speed;

	public float moveSpeed;
	// Use this for initialization
	void Start () {

		player = GameObject.Find ("Player");
		speed = player.GetComponent<ShipMovement>();
		lastX = player.transform.localPosition.x;
		lastY = player.transform.localPosition.y;
		cameraPos = GameObject.Find ("GameCamera").transform;

	}

	
	void Update(){
		this.transform.position = cameraPos.position;

		offsetX =lastX-player.transform.localPosition.x;
		offsetY =lastY - player.transform.localPosition.y;

		lastX = player.transform.localPosition.x;
		lastY = player.transform.localPosition.y;

		UITexture tex = GetComponent<UITexture>();
		
		if (tex != null)
		{
			Rect rect = tex.uvRect;
			rect.x -= offsetX * Time.deltaTime*moveSpeed*speed.speed*0.01f;
			rect.y -= offsetY * Time.deltaTime*moveSpeed*speed.speed*0.01f;
			if(rect.x>1||rect.x<-1){
				rect.x = rect.x- (int)rect.x;
			}	
			if(rect.y>1||rect.y<-1){
				rect.y = rect.y- (int)rect.y;
			}

			tex.uvRect = rect;
		}
	}
}
