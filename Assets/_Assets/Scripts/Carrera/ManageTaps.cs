﻿using UnityEngine;
using System.Collections;
using DarkTonic.MasterAudio;

public class ManageTaps : MonoBehaviour {

	private string lastSide;

	ShipMovement shipMovS;

	GameObject hitObject;
	//int counterForSound=0;
	
	bool leftPressed;
	bool rightPressed;
	float maxTimeForAnotherTouch=0.23f;
	float referenceTime=0;


	// Use this for initialization
	void Start () {
		shipMovS = GameObject.FindWithTag ("Player").GetComponent<ShipMovement> ();

	}
	
	// Update is called once per frame
	void FixedUpdate () {

		referenceTime += Time.deltaTime;
		if (referenceTime > maxTimeForAnotherTouch) {
			referenceTime-=maxTimeForAnotherTouch;

			if(leftPressed&&rightPressed){
				shipMovS.Run("Front");
			} else if(leftPressed&&!rightPressed){
				shipMovS.Run("Left");
			} else if(!leftPressed&&rightPressed){
				shipMovS.Run("Right");
			}
			leftPressed=false;
			rightPressed=false;
		}

	}

	
	void OnTap( TapGesture gesture ) 
	{
		if (CarreraManager.carreraEmpezada) {
			if (gesture.Selection != null) {
				hitObject = gesture.Selection;
				CountTap (hitObject.name);
			}
		}
	}

	private void CountTap(string side){

		/*
		if (lastSide == null) {
			lastSide = side;
		
		} else if (lastSide!=side) {
				shipMovS.Run ();
				lastSide=side;
			if(counterForSound==0){
				MasterAudio.PlaySound ("waterShort"); 
				counterForSound++;
			}else{
				counterForSound=0;
			}
		}
		*/

		if (side == "Left") {
			leftPressed = true;
		} else if (side == "Right") {
			rightPressed = true;
		}
		//shipMovS.Run (side);
	}
}
