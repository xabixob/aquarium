﻿using UnityEngine;
using System.Collections;

public class CarreraPause : MonoBehaviour {

	CarreraManager manager;
	
	void Start()
	{
		manager = GameObject.Find ("GameManager").GetComponent<CarreraManager>();
	}
	
	
	public void OnClick()
	{
		manager.IsPaused = manager.togglePause();
	}
}
