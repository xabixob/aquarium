﻿using UnityEngine;
using System.Collections;
using DarkTonic.MasterAudio;

public class CarreraManager : MonoBehaviour {


	public static bool carreraEmpezada=false;
	public static bool countDownStarted = false;

	GameObject turnInfo;
	GameObject vuelveInfo;
	GameObject metaInfo;
	GameObject gameOver;
	public bool metaAlcanzada=false;
	public bool turnInfoShowed=false;
	float secsOfTurnInfo=5f;
	AchievementManager am;

	public bool IsPaused = false;
	GameObject pauseMenu;
	UISpriteAnimation [] Animations;

	public bool playerVolviendo=false;
	public bool playerVolviendoBoyaPassed = false;
	Transform player;
	Transform whale;

	ShipMovement shipMovementS;

	//Timer data
	GameObject timer;
	float t = 0.0f;

	public const float SHORT_LAP = 2f;
	public const float MEDIUM_LAP= 3f;
	public const float LONG_LAP = 4f;
	public UILabel bestRace;
	public float l;
	public int turboPicked = 0;

	public float raceHalfLength;

	public int turbosTaken = 0;

	const float SQRT3_DIV2 = 0.8660254f;

	//variables GameEnd

	public UILabel tiempoFinal;
	public UILabel textoRanking;
	public GameObject newRecordLabel;


	void Awake(){
		//Screen.orientation = ScreenOrientation.Portrait;
		carreraEmpezada=false;
	}

	// Use this for initialization
	void Start () {
		am = GameObject.Find("AchievementsManager").GetComponent<AchievementManager>();
		turnInfo = GameObject.Find ("TurnInfo");
		vuelveInfo = GameObject.Find ("VuelveInfo");
		metaInfo = GameObject.Find ("MetaInfo");
		gameOver = GameObject.Find ("GameOver");
		player = GameObject.FindWithTag ("Player").transform;
		whale  = GameObject.FindWithTag("Whale").transform;
		shipMovementS = player.gameObject.GetComponent<ShipMovement> ();
		MasterAudio.PlaySound("OceanWaves"); 

		GameObject arrowB = GameObject.Find ("ArrowBoya");
		arrowB.GetComponent<ArrowS> ().SetObjective (GameObject.Find ("BoyaP").transform);

		timer = GameObject.Find("Time");

		pauseMenu = GameObject.Find("Pause");
		Animations = GameObject.Find("GameRoot").GetComponentsInChildren<UISpriteAnimation>(true);




	}

	/*********************************************************************/
	//Set the length of the laps 
	/*********************************************************************/
	public void SetRaceLength(string length)
	{
		GameObject.Find ("BotonBack").SetActive (false);
		l = SHORT_LAP;
		if (length == "MEDIUM") {
			l = MEDIUM_LAP;
			bestRace.text = ((float)(am.tmpValuesArray ["bestbm"].AsInt/100))+"";
		}
		else if (length == "HARD"){
			l = LONG_LAP;
			bestRace.text = ((float)(am.tmpValuesArray ["bestbd"].AsInt/100))+"";
		}
		else {
			bestRace.text = ((float)(am.tmpValuesArray ["bestbf"].AsInt/100))+"" ;
		}
		Vector3 offset = new Vector3(SQRT3_DIV2, 0.5f, 0f)*l;
		//Vector3 tmp = GameObject.Find("Boya").transform.position;
		Vector3 moveBoyaDown = new Vector3(SQRT3_DIV2,-0.5f,0f)*0.15f;
		GameObject.Find("Boya").transform.position = whale.position + moveBoyaDown + offset;//new Vector3(tmp.x, l, tmp.z);
		//tmp = GameObject.Find("BoyaP").transform.position;
		GameObject.Find("BoyaP").transform.position = player.position + moveBoyaDown + offset;

		//raceHalfLength = vector;

	}

	// Update is called once per frame
	void Update () {

		//Pause on/off input (back button)
		if(Input.GetKeyDown(KeyCode.Escape))
			IsPaused = togglePause();

	
	
	/*	float deltaTime = 1.0f/ Time.deltaTime;
		timer.GetComponent<UILabel>().text = "FPS: " + deltaTime.ToString("F2");*/
		if(!metaAlcanzada && carreraEmpezada)
		{
			t = t + Time.deltaTime;
			timer.GetComponent<UILabel>().text = t.ToString("F2");
		}

	}

	public void MarinoSalvado(){
		shipMovementS.IncreseMaxSpeed ();
		shipMovementS.Accelerate();
	
	}


	//UI functions
	public void showTurnInfo(){

		foreach(UITweener tween in turnInfo.GetComponents<UITweener>()){
			tween.PlayForward();
		}

		StartCoroutine (hideTurnInfo());
	}

	IEnumerator hideTurnInfo(){

		yield return new WaitForSeconds(secsOfTurnInfo);
	
		foreach(UITweener tween in turnInfo.GetComponents<UITweener>()){
			tween.enabled=false;
		}
		turnInfo.transform.localScale = Vector3.zero;
	}

	private void showVuelveInfo(){
		
		foreach(UITweener tween in vuelveInfo.GetComponents<UITweener>()){
			tween.PlayForward();
		}
		
		StartCoroutine (hideVuelveInfo());
	}
	
	IEnumerator hideVuelveInfo(){
		
		yield return new WaitForSeconds(secsOfTurnInfo);
		
		foreach(UITweener tween in vuelveInfo.GetComponents<UITweener>()){
			tween.enabled=false;
		}
		vuelveInfo.transform.localScale = Vector3.zero;
	}

	void hideVuelveInfoNOWait(){
		
		foreach(UITweener tween in vuelveInfo.GetComponents<UITweener>()){
			tween.enabled=false;
		}
		vuelveInfo.transform.localScale = Vector3.zero;
	}



	public void showMetaInfo(bool playerWon){
		hideVuelveInfoNOWait();//Hide turn message
		metaAlcanzada = true;
		MasterAudio.PlaySound ("CrowdCheer5s");
		foreach(UITweener tween in metaInfo.GetComponents<UITweener>()){
			tween.PlayForward();
		}
		
		StartCoroutine (hideMetaInfo());
		if(playerWon)
		{
			GameObject.Find ("MetaInfo").transform.localScale= new Vector3(0,0,0);
			tiempoFinal.text = t.ToString("F2");
			if (l == SHORT_LAP){
				if (t < am.tmpValuesArray ["bestbf"].AsInt/100) {
					am.tmpValuesArray ["bestbf"].AsInt = (int)(t*100);
					am.UpdateBestScore(5,am.tmpValuesArray ["bestbf"].AsInt);
					newRecordLabel.SetActive(true);
				}
			}
			else if (l == MEDIUM_LAP){
				if (t < am.tmpValuesArray ["bestbm"].AsInt/100) {
					am.tmpValuesArray ["bestbm"].AsInt = (int)(t*100);
					am.UpdateBestScore(6,am.tmpValuesArray ["bestbm"].AsInt);
					newRecordLabel.SetActive(true);
				}
			}
			else{
				if (t < am.tmpValuesArray ["bestbd"].AsInt/100) {
					am.tmpValuesArray ["bestbd"].AsInt = (int)(t*100);
					am.UpdateBestScore(7,am.tmpValuesArray ["bestbd"].AsInt);
					newRecordLabel.SetActive(true);
				}
			}

			am.tmpValuesArray ["numeroVictorias"].AsInt = am.tmpValuesArray ["numeroVictorias"].AsInt + 1;
			am.tmpValuesArray ["turbosCogidos"].AsInt = am.tmpValuesArray ["turbosCogidos"].AsInt + turboPicked;

			if ((am.achivementsArray ["FirstW"].AsInt < 0)) {
				am.achivementsArray ["FirstW"].AsInt =1;
			}
			if ((am.tmpValuesArray ["numeroVictorias"].AsInt>30) && (am.achivementsArray ["G30Tur"].AsInt < 0)) {
				am.achivementsArray ["G30Tur"].AsInt =1;
			}
			if ((am.tmpValuesArray ["turbosCogidos"].AsInt>50) && (am.achivementsArray ["G50Tur"].AsInt < 0)) {
				am.achivementsArray ["G50Tur"].AsInt =1;
			}
			if ((GetComponent<MarinoManager>().spawnedMarinos == turboPicked) && (am.achivementsArray ["UAllTu"].AsInt < 0)) {
				am.achivementsArray ["UAllTu"].AsInt =1;
			}
			if ((turboPicked==0)  && (am.achivementsArray ["UNoTur"].AsInt < 0)) {
				am.achivementsArray ["UNoTur"].AsInt =1;
			}
			if ((t <30) &&(l == SHORT_LAP) && (am.achivementsArray ["Fac30"].AsInt < 0)) {
				am.achivementsArray ["Fac30"].AsInt =1;
			}
			if ((t <45) &&(l == MEDIUM_LAP) && (am.achivementsArray ["Med45"].AsInt < 0)) {
				am.achivementsArray ["Med45"].AsInt =1;
			}
			if ((t <45) &&(l == LONG_LAP) && (am.achivementsArray ["Dif60"].AsInt < 0)) {
				am.achivementsArray ["Dif60"].AsInt =1;
			}


			am.LoadDataToPlayerPrefs ();
			am.LoadDataToDataBase ();

		}


	}
	
	IEnumerator hideMetaInfo(){
		
		yield return new WaitForSeconds(secsOfTurnInfo);
		
		foreach(UITweener tween in metaInfo.GetComponents<UITweener>()){
			tween.enabled=false;
		}
		metaInfo.transform.localPosition = new Vector3(0,-200f,0);
		GameOver ();
	}

	public void SetPlayerVolviendo(bool b){
		playerVolviendo = b;
		if (b == true && !metaAlcanzada) {
			showVuelveInfo ();
		}
	}

	public void GameOver(){


		gameOver.transform.localScale = new Vector3(1f,1f,1f);
	}

	public bool togglePause()
	{
		pauseMenu.GetComponent<UITweener>().PlayReverse();
		//If already paused -> back to play mode
		if(Time.timeScale == 0f)
		{
			// enable all animations
			foreach(UISpriteAnimation component in Animations)
			{
				component.enabled = true;
			}

			Time.timeScale = 1f;
			return false;
		}
		else if(!metaAlcanzada && (carreraEmpezada || countDownStarted))//If not paused, pause game
		{
			// disable (pause) all animations
			foreach(UISpriteAnimation component in Animations)
			{
				component.enabled = false;
			}

			pauseMenu.GetComponent<UITweener>().PlayForward();
			Time.timeScale = 0f;
			return true;    
		}
		return false;
	}
}
