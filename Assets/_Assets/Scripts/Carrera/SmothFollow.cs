﻿using UnityEngine;
using System.Collections;

public class SmothFollow : MonoBehaviour {

	GameObject player;
	Vector3 offset;
	bool followPlayer=false;
	float objectiveSize=0.35f;
	float startTime =1f;
	float duration = 1.3f;
	bool inPos = false;
	float x;
	public bool startAnim=false;

	// Use this for initialization
	void Start () {
		player = GameObject.Find ("Player");
//		StartCoroutine (IEAnimCameraClose ());
	}
	
	// Update is called once per frame
	void Update () {

		if (!startAnim) 
		{
			startTime = Time.time;
		}
		if (!inPos) 
		{
			float i = (Time.time - startTime) / duration; 
			this.GetComponent<Camera>().orthographicSize = Mathf.Lerp(0.5f, 0.35f, i);

			x = Mathf.Lerp(0, player.transform.localPosition.x, i);
		//	float y = Mathf.Lerp(0, player.transform.localPosition.y, i);
			transform.localPosition= new Vector3(x,/*transform.localPosition.y*/0f,transform.localPosition.z);

			if(this.GetComponent<Camera>().orthographicSize<=0.35f&&transform.localPosition.x<=player.transform.localPosition.x)
			{
				inPos=true;

				transform.localPosition= new Vector3(350f,transform.localPosition.y,transform.localPosition.z);
				this.GetComponent<Camera>().orthographicSize=0.35f;
				
				offset = new Vector3 (0f, transform.localPosition.y - player.transform.localPosition.y, 0f);
				startFollowinPlayer();
		
			}
			
		}

		if (followPlayer) 
		{
			transform.localPosition = player.transform.localPosition + offset;
		}
	}

	public void startFollowinPlayer(){
		followPlayer = true;
	}

	IEnumerator IEAnimCameraClose(){

		while (!inPos) {
		
			GetComponent<Camera>().orthographicSize-=Time.deltaTime*0.1f;
			if(GetComponent<Camera>().orthographicSize<=objectiveSize){
				inPos=true;
				GetComponent<Camera>().orthographicSize=objectiveSize;
				startFollowinPlayer();
			}
		}
		yield return null;
	}
}
