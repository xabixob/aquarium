﻿using UnityEngine;
using System.Collections;

public class MarinoS : MonoBehaviour {

	MarinoManager marinoManager;
	CarreraManager carreraManager;
	void Start () {
		carreraManager = GameObject.Find("GameManager").GetComponent<CarreraManager>();
		marinoManager = GameObject.Find ("GameManager").GetComponent<MarinoManager> ();
	}

	void Update()
	{
		if(carreraManager.metaAlcanzada)
		{
			TweenAlpha.Begin(gameObject, 1.0f, 0.0f);
			StartCoroutine(DeleteDelayed());
		}
	}

	IEnumerator DeleteDelayed()
	{
		yield return new WaitForSeconds(1.0f);
		Recogido();
	}

	public void setArrowTothisMarino(){
		GameObject.Find ("ArrowMarino").GetComponent<ArrowS> ().SetObjective (this.transform);
		GameObject.Find ("ArrowMarino").GetComponentInChildren<UISprite> ().enabled = true;
	}

	public void Recogido(){
		marinoManager.spanwed = false;
		this.gameObject.SetActive(false);
	
	}



}
