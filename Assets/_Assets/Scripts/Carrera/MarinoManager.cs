﻿using UnityEngine;
using System.Collections;

public class MarinoManager : MonoBehaviour {

	public GameObject marino;
	public bool spanwed=false;
	//int maxMarinos=4;
	public int spawnedMarinos=0;
	//Vars para margenes
	float maxDesvioX=600f;
	float maxDistY=1200f;
	float minDistY=600f;
	
	//var pos destino
	float x;
	float y;

	GameObject whale;
	GameObject player;
	CarreraManager carreraManagerS;
	bool IsPlayerFirst = true;
	// Use this for initialization
	void Start () {
		carreraManagerS = GameObject.Find ("GameManager").GetComponent<CarreraManager> ();
		whale = GameObject.Find("Whale");
		player= GameObject.Find("Player");
	}
	
	// Update is called once per frame
	void Update () {
		if(player.transform.position.y < whale.transform.position.y)
		{
			IsPlayerFirst = (carreraManagerS.playerVolviendo)?true:false;
		}
		else
		{
			IsPlayerFirst = (carreraManagerS.playerVolviendo)?false:true;
		}
		if (!spanwed && CarreraManager.carreraEmpezada && !carreraManagerS.metaAlcanzada && !IsPlayerFirst) {
			StartCoroutine(IEwaitToRespawn());
		} 
	}

	void spawnInRandomPos(){
		spawnedMarinos++;
		//bool volviendo = carreraManagerS.playerVolviendo;

		Vector3 playerPos = GameObject.Find ("Player").transform.localPosition;
		Vector3 boyaPos = GameObject.Find ("BoyaP").transform.localPosition;
//		Debug.Log ("Player pos: " + playerPos.y+500);
//		Debug.Log ("boya pos: " + boyaPos.y);

		/*if (carreraManagerS.playerVolviendo||(playerPos.y+500f)>boyaPos.y) {
//			Debug.Log("entra en primero");
			y = Random.Range (playerPos.y - minDistY, playerPos.y - maxDistY);

		} else{
//			Debug.Log ("entra en else");
			y = Random.Range (playerPos.y + minDistY, playerPos.y + maxDistY);
		}
		
		x = Random.Range(boyaPos.x-maxDesvioX/2,boyaPos.x+maxDesvioX);
	*/

		if (carreraManagerS.playerVolviendo||(playerPos.x+500f)>boyaPos.x) {
			x = Random.Range(playerPos.x-500,playerPos.x-600);

			
		} else{
			x = Random.Range(playerPos.x+500,playerPos.x+600);
		}
		
		y = Random.Range (0.52f,0.63f) * x - Random.Range (100, 600);


		Debug.Log ("X: " + x + ",Y: " + y);
		marino.transform.localPosition = new Vector3 (x, y, 0);
		marino.SetActive(true);
		marino.GetComponent<MarinoS> ().setArrowTothisMarino ();


		
	}
	
	IEnumerator IEwaitToRespawn(){
		spanwed = true;
		float randomWaitNumber = Random.Range (4.5f, 7f);
		yield return new WaitForSeconds (randomWaitNumber);
		spawnInRandomPos ();
		
	}
}
