﻿using UnityEngine;
using System.Collections;

public class ProgressBarS : MonoBehaviour {
	//ToGet race status
	CarreraManager carreraManagerS;
	
	//for reference
	Transform player;
	Transform whaleTr;
	WhaleMovement whaleMov;
	
	//TO calculate track lenght
	Transform boyaP;
	Transform boya;
	Transform finishLine;

	//icons to show and vars
	GameObject track;
	GameObject shipIcon;
	GameObject whaleIcon;

	float trackWidth;
	bool whaleReturning=false;
	

	float whaleRelativeX;

	//Data for the progress bar intro and outro
	bool intro = false;//To check if the intro tween has been already called
	//the in-game positions of the icons
	float SIoldLocalPosY;
	float WIoldLocalPosY;
	float ToldLocalPosY;

	float offset = 500.0f;//offset for the icons to get out of the screen
	float tweenTime = 1.5f;//time the icons take for getting into the screen and out of it
	Vector3 initialPlayerPos;
	Vector3 initialWhalePos;
	TweenAlpha twAlpha;
	// Use this for initialization
	void Start () {
		twAlpha = GetComponent<TweenAlpha>();
		carreraManagerS = GameObject.FindWithTag ("GameManager").GetComponent<CarreraManager> ();
		player = GameObject.FindWithTag ("Player").transform;
		initialPlayerPos = player.position;
		whaleTr = GameObject.FindWithTag ("Whale").transform;
		initialWhalePos = whaleTr.position;
		whaleMov = GameObject.FindWithTag ("Whale").GetComponent<WhaleMovement>();
	
		boyaP = GameObject.FindWithTag ("BoyaP").transform;
		finishLine = GameObject.FindWithTag ("Finish").transform;
		boya = GameObject.FindWithTag ("Boya").transform;

		track = GameObject.Find ("Track");
		shipIcon = GameObject.Find ("ShipIcon");
		whaleIcon = GameObject.Find ("WhaleIcon");

		float ratio = Screen.height/2048f;
		trackWidth =/*NI P*** IDEA DE COMO PILLAR EL WIDTH DEL OBJETO TRACK, EN EL MOVIL NO VA*/ GameObject.Find("FinishIcon").transform.localPosition.x - GameObject.Find("Start").transform.localPosition.x;


	}
	
	// Update is called once per frame
	void Update () {
		if(CarreraManager.countDownStarted && !intro)
		{//set the icons in the screen at the beginning of the race
			twAlpha.enabled = true;
			twAlpha.PlayForward();
			/*
			TweenPosition.Begin (shipIcon, tweenTime, new Vector3(shipIcon.transform.localPosition.x, SIoldLocalPosY, 0));
			TweenPosition.Begin (whaleIcon, tweenTime, new Vector3(whaleIcon.transform.localPosition.x, WIoldLocalPosY, 0));
			TweenPosition.Begin (track, tweenTime, new Vector3(track.transform.localPosition.x, ToldLocalPosY, 0));
			*/
		
		}
		if(!carreraManagerS.metaAlcanzada)
		{
			calculateRelativePosition (player);
			calculateRelativePosition (whaleTr);
		}
		else
		{//Once the race finishes remove the progress bar icons from the screen
			twAlpha.PlayReverse();
			/*
			TweenPosition.Begin (shipIcon, tweenTime, new Vector3(shipIcon.transform.localPosition.x, SIoldLocalPosY + offset, 0));
			TweenPosition.Begin (whaleIcon, tweenTime, new Vector3(whaleIcon.transform.localPosition.x, WIoldLocalPosY + offset, 0));
			TweenPosition.Begin (track, tweenTime, new Vector3(track.transform.localPosition.x, ToldLocalPosY + offset, 0));
			*/

		}

	}

	void calculateRelativePosition(Transform obj){

		float RelativeX = 0;
		float halfDistance = Vector3.Distance(boyaP.position,initialPlayerPos);
		//Vector3.Distance(boyaP.localPosition,finishLine.localPosition);

		if (obj.tag == "Player") {
		
			//Compute player position in the line between its start position and the boya which are in front of each
			//other, for later computing the distance
			Vector2 player2dPos = new Vector2(player.position.x, player.position.y);
			Vector2 v = LineIntersectionPoint(player2dPos, player2dPos + new Vector2(-0.866f, 0.5f),
			                                  new Vector2(initialPlayerPos.x, initialPlayerPos.y), new Vector2(boyaP.position.x, boyaP.position.y));

		
			if(!carreraManagerS.playerVolviendoBoyaPassed)
			{
				float d = Vector2.Distance(v, new Vector2(boyaP.position.x, boyaP.position.y))/ halfDistance;
				RelativeX = (d < 0.05f || carreraManagerS.turnInfoShowed)?0f: Mathf.Clamp01(d)*(-trackWidth/2f);
			}
			else
			{
				float d =(1f - Vector2.Distance(v, new Vector2(initialPlayerPos.x, initialPlayerPos.y))/ halfDistance );
				RelativeX = (d > 0.95f)?(trackWidth/2f): Mathf.Clamp01(d)*(trackWidth/2f);
			}
		
			shipIcon.transform.localPosition = new Vector3 (RelativeX, shipIcon.transform.localPosition.y, 0);

		} 
		else if (obj.tag == "Whale") 
		{
			if (!whaleMov.turning && !whaleReturning)
			{
				RelativeX=(Vector3.Distance(obj.position, boya.position) / halfDistance)*(-trackWidth/2);
			}
			else if (whaleMov.turning)
			{
				RelativeX = 0;
				whaleReturning=true;
			}
			else
			{
				RelativeX=(Vector3.Distance(obj.position, boya.position) / halfDistance)*(trackWidth/2);

				Vector2 w = new Vector2(whaleTr.position.x, whaleTr.position.y);
				Vector2 v = LineIntersectionPoint(w, w + new Vector2(-0.866f, 0.5f),
				                                  new Vector2(initialWhalePos.x, initialWhalePos.y), new Vector2(boya.position.x, boya.position.y));
				float d = Vector2.Distance(v, new Vector2(boya.position.x, boya.position.y))/ halfDistance;
				RelativeX =  Mathf.Clamp01(d)*(trackWidth/2f);
			}


			whaleIcon.transform.localPosition = new Vector3 (RelativeX, whaleIcon.transform.localPosition.y, 0);

		}

	}

	Vector2 LineIntersectionPoint(Vector2 ps1, Vector2 pe1, Vector2 ps2, Vector2 pe2)
	{
		// Get A,B,C of first line - points : ps1 to pe1
		float A1 = pe1.y-ps1.y;
		float B1 = ps1.x-pe1.x;
		float C1 = A1*ps1.x+B1*ps1.y;
		
		// Get A,B,C of second line - points : ps2 to pe2
		float A2 = pe2.y-ps2.y;
		float B2 = ps2.x-pe2.x;
		float C2 = A2*ps2.x+B2*ps2.y;
		
		// Get delta and check if the lines are parallel
		float delta = A1*B2 - A2*B1;
		if(delta == 0)
			throw new System.Exception("Lines are parallel");
		
		// now return the Vector2 intersection point
		return new Vector2((B2*C1 - B1*C2)/delta,(A1*C2 - A2*C1)/delta);
	}
}
