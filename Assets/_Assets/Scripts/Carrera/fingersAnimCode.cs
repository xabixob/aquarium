﻿using UnityEngine;
using System.Collections;

public class fingersAnimCode : MonoBehaviour {

	public GameObject Right;
	public GameObject Left;
	bool lanzada=false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (CarreraManager.carreraEmpezada && !lanzada) {
			lanzada=true;
			StartCoroutine(LaunchBothFingers());
		}
	}


	IEnumerator LaunchBothFingers(){
	
		StartCoroutine (showAndHide(Left));
		yield return new WaitForSeconds (0.25f);
		StartCoroutine (showAndHide(Right));


	
	}
	IEnumerator showAndHide(GameObject ob){

		NGUITools.SetActive (ob, true);
		yield return new WaitForSeconds (0.25f);
		NGUITools.SetActive (ob, false);
		yield return new WaitForSeconds (0.25f);
		NGUITools.SetActive (ob, true);

		yield return new WaitForSeconds (0.25f);
		NGUITools.SetActive (ob, false);

		yield return new WaitForSeconds (0.25f);
		NGUITools.SetActive (ob, true);

		yield return new WaitForSeconds (0.25f);
		NGUITools.SetActive (ob, false);

		yield return new WaitForSeconds (0.25f);
		NGUITools.SetActive (ob, true);
		
		yield return new WaitForSeconds (0.25f);
		NGUITools.SetActive (ob, false);



	}
}
