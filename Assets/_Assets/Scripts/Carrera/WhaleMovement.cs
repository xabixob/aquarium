﻿using UnityEngine;
using System.Collections;

public class WhaleMovement : MonoBehaviour {

	public float speed;
	private float normalSpeed;
	public float maxDistance; 

	float distance; 
	float nguiSpeedFactor = 0.0001f;
	private Transform boya;
	public float TurnGradesPerSecond=7.5f;
	Transform player;
	CarreraManager carreraManagerS;
	GameObject meta;
	public bool turning=false;
	bool whaleGoingBack = false;

	UISpriteAnimation anim;
	const float SQRT3_DIV2 = 0.8660254f;
	string [] Animations = {"Whale00", "Whale01", "Whale02", "Whale03", "Whale04", "Whale05", "Whale06", "Whale07"};
	Vector3 [] Directions = {new Vector3(SQRT3_DIV2,0.5f,0f), new Vector3(1,0,0), new Vector3(SQRT3_DIV2,-0.5f,0f), 
		new Vector3(0,-1,0), new Vector3(-SQRT3_DIV2,-0.5f,0f), new Vector3(-1,0,0), 
		new Vector3(-SQRT3_DIV2,0.5f,0f), new Vector3(0,1,0)};
	int index = 0;


	Vector3 dir;
	// Use this for initialization
	void Start () {
		dir = new Vector3(SQRT3_DIV2, 0.5f, 0f);
		anim = GetComponent<UISpriteAnimation>();
		player = GameObject.FindWithTag ("Player").transform;
		boya = GameObject.Find ("Boya").transform;
		normalSpeed = speed;
		carreraManagerS = GameObject.FindWithTag ("GameManager").GetComponent<CarreraManager> ();
		meta = GameObject.Find ("FinishLine");

		Physics.IgnoreCollision(GetComponent<Collider>(), GameObject.Find("Whale").GetComponent<Collider>());
	}
	
	// Update is called once per frame
	void Update () {

		if (CarreraManager.carreraEmpezada)
		{
			anim.framesPerSecond= (int)(speed/300);

			distance = Vector3.Distance (transform.position, player.position);


			if (distance > maxDistance) 
			{

				if (speed > player.gameObject.GetComponent<ShipMovement> ().speed) 
				{
					speed = player.gameObject.GetComponent<ShipMovement> ().speed - 200f;
				} 
				else 
				{

					speed = player.gameObject.GetComponent<ShipMovement> ().speed - 30f;
					if (speed < 250f) 
					{
						speed = 250f;
					}

				}
			}
			else 
			{
				speed = normalSpeed;
			}

			float d = Vector3.Distance(transform.localPosition, boya.localPosition);

			//Slow down if getting close to goal
			if(d < 450f)
			{
				speed -= 100f;
				Mathf.Clamp(speed, 100f, 300f);
			}

			transform.Translate (dir * speed * nguiSpeedFactor * Time.deltaTime);	


			switch(index)
			{
				case 0:
				{
					if(d < 160f)
						ChangeWhaleDirection();
					break;
				}
				case 1:
				{
					if(transform.localPosition.x > boya.localPosition.x)
					{
						ChangeWhaleDirection();
						turning = true;
					}
					break;
				}
				case 2:
				{
					if(transform.localPosition.y < boya.localPosition.y)
						ChangeWhaleDirection();
					break;
				}
				case 3:
				{
					if(d > 190f)
					{
						ChangeWhaleDirection();
						turning = false;
						whaleGoingBack = true;
					}	
					break;
				}
			}
		

		

			/*if (carreraManagerS.turnInfoShowed && transform.localPosition.y-256f < meta.transform.localPosition.y && carreraManagerS.metaAlcanzada == false) {

				carreraManagerS.showMetaInfo (false);
			}*/
		}
	}
	void ChangeWhaleDirection()
	{
		index++;
		dir = Directions[index];
		anim.namePrefix = Animations[index];
	}
	void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.CompareTag("Finish") && /*carreraManagerS.turnInfoShowed*/whaleGoingBack && carreraManagerS.metaAlcanzada == false)
		{
			carreraManagerS.showMetaInfo (false);
		}

	}

}
