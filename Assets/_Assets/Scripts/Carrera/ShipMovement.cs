﻿using UnityEngine;
using System.Collections;
using DarkTonic.MasterAudio;

public class ShipMovement : MonoBehaviour {

	public GameObject collider;
	public float speed;
	public float speedIncrementPerTap;
	public float nguiSpeedFactor = 0.0001f;
	public float maxSpeed;
	public float freno;
//	Vector3 direction;
	public float turnSpeed;
	public float turnGrades;
	public float turningTime;
	public float lerpSpeed=1f;
//	float turningFactor =0; //turningFactor
	CarreraManager carreraManagerS;
	//GameObject meta;
	string previousSide;

	// Elementos GameEnd
	public UILabel mensajeEnd;


	UILabel speedLabel;
	UISpriteAnimation animation;

	int acc = 0;
	//const float ONE_DIV_SQRT2 = 0.70710678f;
	const float SQRT3_DIV2 = 0.8660254f;
	string [] Animations = {"Bote00", "Bote01", "Bote02", "Bote03", "Bote04", "Bote05", "Bote06", "Bote07"};
	Vector3 [] Directions = {new Vector3(SQRT3_DIV2,0.5f,0f), new Vector3(1,0,0), new Vector3(SQRT3_DIV2,-0.5f,0f), 
								new Vector3(0,-1,0), new Vector3(-SQRT3_DIV2,-0.5f,0f), new Vector3(-1,0,0), 
								new Vector3(-SQRT3_DIV2,0.5f,0f), new Vector3(0,1,0)};
	int animationIndex = 0;
	// Use this for initialization
	void Start ()
	{
		animation = GetComponent<UISpriteAnimation>();
		/*foreach(Vector3 v in Directions)
		{
			v.Normalize();
		}*/

		//direction = new Vector3 (0, 1, 0);
		carreraManagerS = GameObject.FindWithTag ("GameManager").GetComponent<CarreraManager> ();
		//meta = GameObject.Find ("FinishLine");
		speedLabel = GameObject.Find("Speed").GetComponent<UILabel>();
		previousSide = "";
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		//Hack sucio-> avoid "Bote" with no numbers and having animation go through all "Bote00_...", "Bote01_..." etc animations
		if(!animation.namePrefix.Contains ("Bote0"))
			animation.namePrefix = Animations[animationIndex];
		if (CarreraManager.carreraEmpezada) 
		{
			GetComponent<UISpriteAnimation>().framesPerSecond=(int) (speed/300);

			if (speed > 0) 
			{
				speed -= freno * Time.deltaTime;
			}
			if (speed < 0) 
			{
				speed = 0;
			}

			/*if (carreraManagerS.turnInfoShowed && transform.localPosition.y-128f < meta.transform.localPosition.y && carreraManagerS.metaAlcanzada == false&&carreraManagerS.playerVolviendo) {
				GameObject.Find ("MetaInfo").GetComponentInChildren<UILabel> ().text = "IRABAZI DUZU!!";
				carreraManagerS.showMetaInfo (true);
			}*/
			if(acc > 0)
			{
				speed *= 2.0f;
			}

		//	transform.position	=Vector3.Lerp(transform.position,transform.position+Vector3.up * speed * nguiSpeedFactor,lerpSpeed * Time.deltaTime);

		//	transform.Translate (Vector3.up * speed * nguiSpeedFactor/10 * Time.deltaTime);	

			//Everything done step by step for debugging purposes
			float scaleVector = speed * nguiSpeedFactor/10 * Time.deltaTime;
			Vector3 tr = Directions[animationIndex];
			tr *= scaleVector;
			transform.Translate (tr);	
			speedLabel.text = speed.ToString();
			if(acc > 0)
			{
				acc--;
				speed /= 2.0f;
			}
		}
	}

	public void Run(string side){
		if (CarreraManager.carreraEmpezada && !carreraManagerS.metaAlcanzada) 
		{
			float ratio=1;

			if(side=="Left" && previousSide == side){
				MoveLeft();
				ratio=0.75f;
			}else if(side=="Right" && previousSide == side){
				MoveRight();
				ratio=0.75f;
			}else if(side=="Front" || (side == "Left" && previousSide == "Right") || (side == "Right" && previousSide == "Left")){
				MoveFront();
			}
			previousSide = side;
			if (speed < maxSpeed) {
				speed += speedIncrementPerTap*ratio;
			} else if (speed == 0) {
				speed = 300;
			}
		}
	}
	public void MoveLeft(){
		animationIndex--;
		animationIndex = ((animationIndex%8) + 8)% 8;
		animation.namePrefix = Animations[animationIndex];
		SetCollider(animationIndex);
	//	transform.Rotate (0f, 0f, turnGrades);

		//transform.Translate (Vector3.up * speed * nguiSpeedFactor * Time.deltaTime*0.75f);	
		MasterAudio.PlaySound("waterLong"); 

		//transform.Translate (-Vector3.right * speed * nguiSpeedFactor * Time.deltaTime);	
	}
	public void MoveRight(){
		animationIndex++;
		animationIndex = ((animationIndex%8) + 8)% 8;
		animation.namePrefix = Animations[animationIndex];
		SetCollider(animationIndex);
		//transform.Rotate (0f, 0f, -turnGrades);
		//transform.Translate (Vector3.up * speed * nguiSpeedFactor * Time.deltaTime*0.75f);	
		MasterAudio.PlaySound("waterLong"); 

		//transform.Translate (Vector3.right * speed * nguiSpeedFactor * Time.deltaTime);	

	}

	public void MoveFront(){

	}

	public void Accelerate()
	{
		acc = 180;
	}
	public void IncreseMaxSpeed(){
		maxSpeed = maxSpeed + 200;
	
	}

	void OnTriggerExit(Collider other) {
//		Debug.Log ("OnTriggerExit");
		if (other.gameObject.CompareTag ("BoyaP")) {
			//if (transform.localPosition.y > GameObject.FindWithTag ("BoyaP").transform.localPosition.y) {
				carreraManagerS.SetPlayerVolviendo (true);
				Debug.Log ("OnTriggerExit BoyaP");

			//}
		}
		else if (other.gameObject.CompareTag ("GoBack")) 
		{
			if(!carreraManagerS.turnInfoShowed)
			{
				carreraManagerS.turnInfoShowed=true;
				carreraManagerS.showTurnInfo ();
			}
			else
			{
				carreraManagerS.playerVolviendoBoyaPassed = true;
			}
		}
	}

	void OnTriggerEnter(Collider other){

		if (other.gameObject.CompareTag("Marino")) {
			carreraManagerS.turbosTaken++;
			/*****************************************************/

			/*****************************************************/
			carreraManagerS.MarinoSalvado();
			other.GetComponent<MarinoS>().Recogido();
			
		}
		else if(other.gameObject.CompareTag("Finish") && carreraManagerS.turnInfoShowed && carreraManagerS.metaAlcanzada == false&&carreraManagerS.playerVolviendo)
		{
			GameObject.Find ("MetaInfo").GetComponentInChildren<UILabel> ().text = Language.Get("YOU_WON");
			carreraManagerS.showMetaInfo (true);
			carreraManagerS.transform.localScale = new Vector3(0,0,0);
			mensajeEnd.text = Language.Get("YOU_WON");
		}

	}

	void SetCollider(int index)
	{
		CapsuleCollider c = collider.GetComponent<CapsuleCollider>();
		switch (index)
		{
			case 0:
			{
		
			collider.transform.eulerAngles = new Vector3(0f,0f, -40f);
				c.center = new Vector3(-15f, 20f, 0);
				c.radius = 30f;
				c.height = 136f;
				break;
			}
			case 1:
			{
			collider.transform.eulerAngles = new Vector3(0f,0f, -80f);
				c.center = new Vector3(-20f, 1f, 0);
				c.radius = 30f;
				c.height = 136f;
				break;
			}
			case 2:
			{
			collider.transform.eulerAngles = new Vector3(0f,0f, 70f);
				c.center = new Vector3(8f, 16f, 0);
				c.radius = 30f;
				c.height = 120f;
				break;
			}
			case 3:
			{
			collider.transform.eulerAngles = new Vector3(0f,0f, 0f);
				c.center = new Vector3(-4f, 13f, 0);
				c.radius = 30f;
				c.height = 107f;
				break;
			}
			case 4:
			{
			collider.transform.eulerAngles = new Vector3(0f,0f, -70f);
				c.center = new Vector3(-20f, 0, 0);
				c.radius = 30f;
				c.height = 120f;
				break;
			}
			case 5:
			{
			collider.transform.eulerAngles = new Vector3(0f,0f, 75f);
				c.center = new Vector3(16f, 17f, 0);
				c.radius = 30f;
				c.height = 136f;
				break;
			}
			case 6:
			{
			collider.transform.eulerAngles = new Vector3(0f,0f, 70f);
				c.center = new Vector3(15f, 20f, 0);
				c.radius = 30f;
				c.height = 130f;
				break;
			}
			case 7:
			{
			collider.transform.eulerAngles = new Vector3(0f,0f, 0f);
				c.center = new Vector3(-5f, 28f, 0);
				c.radius = 30f;
				c.height = 130f;
				break;
			}
		}
	}

}
