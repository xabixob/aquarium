﻿using UnityEngine;
using System.Collections;

public class ArrowS : MonoBehaviour {

	Transform arrow;
	Transform player;
	Transform objective;
	Vector3 direction;
	float distanceToTop;
	float distanceToBottom;
	float gameCameraY;

	Vector3 posicionFinal= new Vector3();
	public float lerpSpeed=1f;


	// Use this for initialization
	void Start () {

		gameCameraY = GameObject.Find ("GameCamera").transform.localPosition.y;
		arrow= this.transform;
		player = GameObject.Find/*WithTag*/ ("Player").transform;
		this.transform.position = player.position;
		distanceToTop = 1024 - gameCameraY;
		distanceToBottom = gameCameraY;
		this.gameObject.GetComponentInChildren<UISprite>().enabled=false;
		if (this.name == "ArrowBoya") {
			SetObjective(GameObject.FindWithTag ("BoyaP").transform);
		}

	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (this.gameObject.GetComponentInChildren<UISprite> ().enabled) {
			SetArrowPos ();
			transform.localPosition = Vector3.Lerp(transform.localPosition, posicionFinal, lerpSpeed * Time.deltaTime);
		}
	}

	void SetArrowPos(){
		//position
		direction = player.localPosition - objective.localPosition;
		float bigX = objective.localPosition.x - player.localPosition.x;
		float bigY = (objective.localPosition.y - player.localPosition.y);

		float x = distanceToTop * bigX / bigY;
		Vector3 dir;

		float xDestino=0;
		float yDestino=0;

		if (objective.localPosition.y > player.localPosition.y) 
		{
			if (bigY < distanceToTop*0.66f) 
			{
				xDestino = objective.localPosition.x;
				yDestino = objective.localPosition.y;
				//arrow.localPosition = new Vector3 (objective.localPosition.x, objective.localPosition.y, 0);

			} 
			else if(bigY >distanceToTop*0.66f)
			{
				
				xDestino = (player.localPosition.x + x);
				yDestino = player.localPosition.y + (distanceToTop-300f);//210f);// - 300f;
				//arrow.localPosition = new Vector3 (player.localPosition.x+ x, player.localPosition.y + distanceToTop-300f, 0);
			}
		} 
		else 
		{
			if (objective.localPosition.y < player.localPosition.y-distanceToBottom*0.66f) 
			{
				xDestino = objective.localPosition.x;
				yDestino = player.localPosition.y-gameCameraY+100f;
				//arrow.localPosition = new Vector3 (objective.localPosition.x, objective.localPosition.y, 0);
				
			} 
			else
			{
				xDestino = player.localPosition.x + x;
				yDestino = objective.localPosition.y;// -distanceToBottom+50f;
				//arrow.localPosition = new Vector3 (player.localPosition.x+ x, player.localPosition.y + distanceToTop-300f, 0);
			}
		
		}
		dir = objective.position - player.position;
		posicionFinal = new Vector3 (xDestino, yDestino, 0);

		//rotation
	
		float angle = Mathf.Atan2(dir.y,dir.x) * Mathf.Rad2Deg;
		arrow.rotation = Quaternion.AngleAxis(angle, Vector3.forward);


		if ((direction.magnitude < 330 && objective.localPosition.y > player.localPosition.y)||(direction.magnitude < 150 && objective.localPosition.y < player.localPosition.y)) {
			this.gameObject.GetComponentInChildren<UISprite> ().enabled = false;
		} 
	}

	public void SetObjective(Transform obj){

		objective = obj;
//		direction = player.localPosition - obj.localPosition;
		this.gameObject.GetComponentInChildren<UISprite>().enabled=true;
		
	}


}
