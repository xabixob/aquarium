﻿using UnityEngine;
using System.Collections;
using DarkTonic.MasterAudio;

public class CountDown : MonoBehaviour {

	GameObject titulo;
	GameObject press;
	UILabel texto;
	UITweener tween;
	CarreraManager cm;
	// Use this for initialization
	void Start () {
		cm = GameObject.Find("GameManager").GetComponent<CarreraManager>();
		texto = GetComponent<UILabel> ();
		tween = GetComponent<UITweener> ();
		titulo = GameObject.Find ("Titulo");
		press = GameObject.Find ("Press");
	}
	


	public void StartCountdown(){
		titulo.transform.localScale=new Vector3 (0f, 0f, 0f);
		press.GetComponent<UITweener> ().enabled = false;
		press.transform.localScale=new Vector3 (0f, 0f, 0f);

		transform.localScale = new Vector3 (1f, 1f, 1f);
		CarreraManager.countDownStarted = true;
		StartCoroutine (count ());

	}

	IEnumerator count(){
		tween.ResetToBeginning ();
		tween.PlayForward ();
		MasterAudio.PlaySound ("ShorTone");
		yield return new WaitForSeconds (1);
		//start posicionando Gamecamera
		GameObject.Find ("GameCamera").GetComponent<SmothFollow> ().startAnim = true;

		texto.text = "2";
		tween.ResetToBeginning ();
		tween.PlayForward ();
		MasterAudio.PlaySound ("ShorTone");
		yield return new WaitForSeconds (1);

		texto.text = "1";
		tween.ResetToBeginning ();
		tween.PlayForward ();
		MasterAudio.PlaySound ("ShorTone");
		yield return new WaitForSeconds (1);

		texto.text = "GO!";
		tween.ResetToBeginning ();
		tween.PlayForward ();
		MasterAudio.PlaySound ("GoTone");

		GameObject.Find("StartMenu").GetComponent<TweenScale>().PlayForward();

		CarreraManager.carreraEmpezada = true;
		GameObject.FindWithTag("GameInfo").GetComponent<TweenAlpha>().enabled = true;
		CarreraManager.countDownStarted = false;

		//Print best time
		float bt;

		if(cm.l == CarreraManager.MEDIUM_LAP)
		{
			bt = 0;
		}
		else if(cm.l == CarreraManager.LONG_LAP)
		{
			bt = 0;
		}
		else
		{
			bt = 0;
		}
		/*string onena = "- -";
		if(bt > 0)
			onena = bt.ToString("F2");
		GameObject.Find("BestTime").GetComponent<UILabel>().text = onena;*/
		//GameObject.Find("BestTime").GetComponent<TweenScale>().PlayForward();
		//Print time (0.00)
		float t = 0f;
		GameObject.Find("Time").GetComponent<UILabel>().text = t.ToString("F2");
		//GameObject.Find("Time").GetComponent<TweenScale>().PlayForward();
	}
}
