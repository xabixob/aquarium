﻿using UnityEngine;
using System.Collections;


public class ChooseLocalization : MonoBehaviour {
	public UISprite EUSprite;
	public UISprite ENSprite;
	public UISprite ESSprite;
	public UISprite FRSprite;
	void Awake()
	{
		EUSprite.spriteName = "Euskera_Neutro";
		ENSprite.spriteName = "Ingles_Neutro";
		ESSprite.spriteName = "Spanish_Neutro";
		FRSprite.spriteName = "French_Neutro";
		switch(Language.CurrentLanguage())
		{
		case LanguageCode.EN:
		{
			ENSprite.spriteName = "Ingles_Select";
			break;
		}
		case LanguageCode.ES:
		{
			ESSprite.spriteName = "Spanish_Select";
			break;
		}
		default:
		{
			EUSprite.spriteName = "Euskera_Select";
			break;
		}
		}
	}
	public void SetLanguage(string language)
	{
		StartCoroutine(ChangeLanguage(language));

		LanguageCode l = LanguageCode.EU;
		if(language == "Español")
			l = LanguageCode.ES;
		else if(language == "English")
			l = LanguageCode.EN;


		Language.SwitchLanguage(l);
		Application.LoadLevel ("menu");
	}

	IEnumerator ChangeLanguage(string str)
	{
		string url = Constants.URL+"updateUserData.php?id="+PlayerPrefs.GetInt("playerId")+"&field=Language&data="+str;
		WWW download = new WWW(url);
		Debug.Log("URL: "+url);
		
		// Wait until the download is done
		yield return download;
		
		if(download.error != null) {
			print( "Error downloading: " + download.error );
			yield return null;
		} else {

			Debug.Log("Language Updated.");
		}
	}
}
