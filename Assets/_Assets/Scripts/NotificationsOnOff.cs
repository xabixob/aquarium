﻿using UnityEngine;
using System.Collections;

public class NotificationsOnOff : MonoBehaviour {
	public UILabel text;
	void Awake()
	{
		if (PlayerPrefs.GetInt ("notifications") == 1) {
			text.text = Language.Get("NOTIFICATIONS") + " ON";
		} else {
			text.text = Language.Get("NOTIFICATIONS") + " OFF";
		}
	}
	public void ChangeNotification()
	{
		if (PlayerPrefs.GetInt ("notifications") == 1) {
			PlayerPrefs.SetInt("notifications",0);
			text.text = Language.Get("NOTIFICATIONS") + " OFF";
		} else {
			PlayerPrefs.SetInt("notifications",1);
			text.text = Language.Get("NOTIFICATIONS") + " ON";
		}

	}


}
