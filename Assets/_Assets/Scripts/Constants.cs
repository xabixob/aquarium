﻿using UnityEngine;
using System.Collections;

public class Constants : MonoBehaviour {

	public static string URL= "http://192.168.1.20/acuarium/";

	public static string DEFAULT_ACHIVEMENTS = "{\"g100p\":\"-1\",\"dAllSp\":\"-1\",\"d30Sp\":\"-1\",\"d100Sp\":\"-1\",\"m500sh\":\"-1\",\"d50Ru\":\"-1\",\"d150Ru\":\"-1\",\"s500B\":\"-1\",\"50of80\":\"-1\",\"75of80\":\"-1\",\"100of80\":\"-1\",\"4less15\":\"-1\",\"4less10\":\"-1\",\"4less5\":\"-1\",\"4AtOnce\":\"-1\",\"8less15\":\"-1\",\"8less10\":\"-1\",\"8less5\":\"-1\",\"8AtOnce\":\"-1\",\"12less10\":\"-1\",\"12less15\":\"-1\",\"FirstW\":\"-1\",\"G50Tur\":\"-1\",\"G30Tur\":\"-1\",\"UAllTu\":\"-1\",\"UNoTur\":\"-1\",\"Fac30\":\"-1\",\"Med45\":\"-1\",\"Dif60\":\"-1\",\"ARImg1\":\"-1\",\"ARImg2\":\"-1\",\"ARImg3\":\"-1\",\"ARImg4\":\"-1\",\"ARImg5\":\"-1\",\"ARImg6\":\"-1\",\"ARImg7\":\"-1\",\"ARImg8\":\"-1\",\"ARImg9\":\"-1\",\"ARImg10\":\"-1\",\"ARImg11\":\"-1\",\"ARImg12\":\"-1\",\"ARImg13\":\"-1\",\"ARImg14\":\"-1\",\"ARImg15\":\"-1\"}";
	public static string DEFAULT_GAME_DATA = "{\"numEspecialesTotal\":\"0\",\"tirosFallados\":\"0\",\"tirosHechos\":\"0\",\"basuraDestruida\":\"0\",\"turbosCogidos\":\"0\",\"numeroVictorias\":\"0\",\"bestkr\":\"0\",\"bestpf\":\"0\",\"bestpm\":\"0\",\"bestpd\":\"0\",\"bestbf\":\"0\",\"bestbm\":\"0\",\"bestbd\":\"0\"}";
	public static string[] BACKGROUND_MEDALS = {"Bronce","Bronce","Plata","Oro","Generico","Plata","Oro","Generico","Bronce","Plata","Oro","Bronce","Plata","Oro","Plata","Bronce","Plata","Oro","Oro","Plata","Oro","Bronce","Bronce","Oro","Plata","Bronce","Bronce","Plata","Oro","Generico","Generico","Generico","Generico","Generico","Generico","Generico","Generico","Generico","Generico","Generico","Generico","Generico","Generico","Generico"};
	public static string[] CONTENIDO_MEDALS = {"ICO_Acierto","ICO_Boya","ICO_Boya","ICO_Boya","ICO_DisparoFallo","ICO_Acierto","ICO_Acierto","ICO_Disparo","ICO_DisparoFallo_por","ICO_DisparoFallo_por","ICO_DisparoFallo_por","ICO_Tiempo_easy","ICO_Tiempo_easy","ICO_Tiempo_easy","ICO_Barril","ICO_Tiempo_med","ICO_Tiempo_med","ICO_Tiempo_med","ICO_Barril","ICO_Tiempo_hard","ICO_Tiempo_hard","ICO_Ganar","ICO_Turbo","ICO_Ganar","ICO_Turbo","ICO_Turbo","ICO_Ganar_best","ICO_Ganar_best","ICO_Ganar_best","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15"};
}
