﻿using UnityEngine;
using System.Collections;

public class AchivementsListManager : MonoBehaviour {

	private AchievementManager am;
	public UIScrollBar barradesl;
	// Use this for initialization
	void Start () {
		am = GameObject.Find("AchievementsManager").GetComponent<AchievementManager>();
		UpdateAchivementListStatus ();
		barradesl.value = am.selectedMedal / 45f;
	}
	
	public void UpdateAchivementListStatus(){
		for (int i=0; i<am.achivementsArray.Count; i++) {
			if (am.achivementsArray[i].AsInt>0){
				GameObject.Find("Logro"+(i+1)).GetComponent<UISprite>().color = new Color(1,1,1,1);
			}
		}
	}
}
