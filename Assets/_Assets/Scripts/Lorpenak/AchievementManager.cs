﻿using UnityEngine;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary; 
using System.IO;
using System.Collections.Generic;
//using Prime31;
using SimpleJSON;

public class AchievementManager : MonoBehaviour {

	public JSONNode achivementsArray;
	public JSONNode tmpValuesArray;
	public int  selectedMedal = 0;

	void Start(){
		DontDestroyOnLoad (gameObject);

		if (PlayerPrefs.GetInt ("playerId") != 0) {
			if (!PlayerPrefs.HasKey ("achivement") || !PlayerPrefs.HasKey ("gameinfo")) {
				StartCoroutine ("LoadAchivementInformation");
			} else {
				LoadDataFromPlayerPrefs ();
				GameObject.Find ("MedalManager").GetComponent<UpdateMedals> ().UpdateMedallasList ();
			}
		} else {
			LoadNewWarningMessage("WARNIN_3");
			PlayerPrefs.SetString("achievements",Constants.DEFAULT_ACHIVEMENTS);
			PlayerPrefs.SetString("gameinfo",Constants.DEFAULT_GAME_DATA);
			LoadDataFromPlayerPrefs();

			GameObject.Find ("MedalManager").GetComponent<UpdateMedals> ().UpdateMedallasList ();

		}
	}

	public IEnumerator LoadAchivementInformation(){
		string url = Constants.URL+"loadAchivements.php?usu="+PlayerPrefs.GetInt ("playerId");
		WWW download = new WWW(url);

		
		yield return download;
		
		if(download.error != null) {
			yield return null;
		} else {
			JSONNode tmpresp = SimpleJSON.JSONArray.Parse(download.text);
			if (int.Parse(tmpresp["resp"]) == 1){
				PlayerPrefs.SetString("achievements",tmpresp["arrayAchivements"]);
				PlayerPrefs.SetString("gameinfo",tmpresp["arrayScores"]);
				LoadDataFromPlayerPrefs();
			}
			else{
				PlayerPrefs.SetString("achievements",Constants.DEFAULT_ACHIVEMENTS);
				PlayerPrefs.SetString("gameinfo",Constants.DEFAULT_GAME_DATA);
				LoadDataFromPlayerPrefs();
			}
			GameObject.Find("MedalManager").GetComponent<UpdateMedals>().UpdateMedallasList();
		}
	}

	public void LoadDataFromPlayerPrefs(){

		achivementsArray = SimpleJSON.JSONArray.Parse (PlayerPrefs.GetString ("achievements"));

		tmpValuesArray = SimpleJSON.JSONArray.Parse (PlayerPrefs.GetString ("gameinfo"));

	}

	public void LoadDataToDataBase(){
		StartCoroutine ("LoadDataToDataBaseRoutine");
	}
	public IEnumerator LoadDataToDataBaseRoutine(){
		if (PlayerPrefs.GetInt ("playerId") > 0) {
			string url = Constants.URL + "saveAllData.php?usu=" + PlayerPrefs.GetInt ("playerId");
			WWWForm info = new WWWForm ();
			info.AddField ("achievements", PlayerPrefs.GetString ("achievements"));
			info.AddField ("gameinfo", PlayerPrefs.GetString ("gameinfo"));
			WWW download = new WWW (url, info);

		
			yield return download;
		
			if (download.error != null) {
				yield return null;
			} else {
				JSONNode tmpresp = SimpleJSON.JSONArray.Parse (download.text);

			}
		}
	}

	public void LoadDataToPlayerPrefs(){

		PlayerPrefs.SetString ("achievements", achivementsArray.ToString ());
		PlayerPrefs.SetString ("gameinfo", tmpValuesArray.ToString ());
	}

	public void UpdateBestScore(int gameId,int score){
		StartCoroutine (UpdateBestScoreCoroutine (gameId, score));
	}

	private IEnumerator UpdateBestScoreCoroutine(int gId,int sco){
		if (PlayerPrefs.GetInt ("playerId") > 0) {
			string url = Constants.URL + "saveScore.php?usu=" + PlayerPrefs.GetInt ("playerId");
			WWWForm info = new WWWForm ();
			info.AddField ("idGame", gId);
			info.AddField ("score", sco);
			WWW download = new WWW (url, info);

		
			yield return download;
		
			if (download.error != null) {
				yield return null;
			} else {
				JSONNode tmpresp = SimpleJSON.JSONArray.Parse (download.text);
			
			}
		}
	}

	private void LoadNewWarningMessage(string mess){
		GameObject.Find ("UI Root").transform.FindChild ("ErrorMessage/Panel/Messaje").GetComponent<UILabel> ().text = Language.Get (mess);
		GameObject.Find ("UI Root").transform.FindChild ("ErrorMessage").gameObject.SetActive (true);
		GameObject.Find ("UI Root").transform.FindChild ("ErrorMessage/Panel").GetComponent<TweenScale> ().PlayForward ();
	}

}
