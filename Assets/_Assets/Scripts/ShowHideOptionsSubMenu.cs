﻿using UnityEngine;
using System.Collections;

public class ShowHideOptionsSubMenu : MonoBehaviour {
	TweenAlpha tw;
	UITexture[] childrenTextures;
	void Awake()
	{
		GetComponent<UIButton>().defaultColor = GetComponent<UITexture>().color;
		//GetComponent<UITexture>().enabled = true;
	}
	void Start()
	{
		tw = GetComponent<TweenAlpha>();
		childrenTextures = GetComponentsInChildren<UITexture>(true);
	
	}

	public void ShowSubMenu()
	{
		tw.PlayForward();
		StartCoroutine(SetActiveDelayed(tw.delay, true));
	}
	public void HideSubmenu()
	{
		tw.PlayReverse();
		StartCoroutine(SetActiveDelayed(tw.delay, false));
	}

	IEnumerator SetActiveDelayed(float t, bool a)
	{
		yield return new WaitForSeconds(t);
		GetComponent<UIButton>().enabled = a;
		GetComponent<UITexture>().enabled = a;

		foreach(UITexture texture in childrenTextures)
		{
			texture.enabled = a;
		}
	}
}
