﻿using UnityEngine;
using System.Collections;

public class TextLocalization : MonoBehaviour 
{
	public string Text;
	void Awake () 
	{
		GetComponent<UILabel>().text = Language.Get(Text);
	}

}
