using UnityEngine;
using System.Collections;

public class AntxoniAnim : MonoBehaviour {


	UISpriteAnimation anim;
	// Use this for initialization

	void Start () {
		anim = GetComponentInChildren<UISpriteAnimation> ();
		//	animation.framesPerSecond = 16;
		anim.ResetToBeginning ();
		//anim.namePrefix="Entrada_";
		//anim.Play();
	}
	
	// Update is called once per frame
	void Update () {

		if (anim.isPlaying) {
		} else {
		//	animation.framesPerSecond=16;
			anim.namePrefix="Idle_";
			anim.loop=true;
			anim.Play();
		}
	}
}
