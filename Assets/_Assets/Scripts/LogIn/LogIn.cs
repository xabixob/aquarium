﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//using Prime31;



public class PlayerData
{
	public PlayerData(){}
	public string id;
	public string name;
	public string email;
	public string language;
	public bool notification;

	public string GetNameWithSpaces()
	{
		return name.Replace('_', ' ');
	}
}
public class LogIn : MonoBehaviour {
	PlayerData pd;
	//private string _text = "(null)";

	void Awake()
	{
		//PlayerPrefs.DeleteAll ();
		if (PlayerPrefs.HasKey ("playerId") ) {
			if(PlayerPrefs.GetInt ("playerId") != 0){
				Application.LoadLevel("menu");
			}
		}
#if UNITY_ANDROID
		InitializeGCM();
		if(FacebookAndroid.isSessionValid())
		{Debug.Log("awake logged");
			LogInSuccess();//Application.LoadLevel ("menu");
		}

		void InitializeGCM()
		{
			// Create receiver game object
			GCM.Initialize ();
			
			// Set callbacks
			GCM.SetErrorCallback ((string errorId) => {
				Debug.Log ("Error!!! " + errorId);
				//GCM.ShowToast ("Error!!!");
				//_text = "Error: " + errorId;
			});
			
			GCM.SetMessageCallback ((Dictionary<string, object> table) => {
				Debug.Log ("Message!!!");
				
				//GCM.ShowToast ("Message!!!"+ table.ToString ());
				/*_text = "Message: " + System.Environment.NewLine;
			foreach (var key in  table.Keys) {
				_text += key + "=" + table[key] + System.Environment.NewLine;
			}*/
				//GCM.ShowToast (_text);
			});
			
			GCM.SetRegisteredCallback ((string registrationId) => {
				Debug.Log ("Registered!!! " + registrationId);
				//GCM.ShowToast ("Registered!!!");
				//_text = "Register: " + registrationId; 
			});
			
			GCM.SetUnregisteredCallback ((string registrationId) => {
				Debug.Log ("Unregistered!!! " + registrationId);
				//GCM.ShowToast ("Unregistered!!!");
				//_text = "Unregister: " + registrationId;
			});
			
			GCM.SetDeleteMessagesCallback ((int total) => {
				Debug.Log ("DeleteMessages!!! " + total);
				//GCM.ShowToast ("DeleteMessaged!!!");
				//_text = "DeleteMessages: " + total;
			});
			
			GCM.Register ("279996016929");
			//string gcmID =	GCM.GetRegistrationId();
			
			
		}
#elif UNITY_IPHONE

#endif

	}


	void Start () 
	{
#if UNITY_ANDROID
		FacebookAndroid.init();
		FacebookManager.sessionOpenedEvent += LogInSuccess;
		FacebookManager.loginFailedEvent += loginFailedEvent;
#elif UNITY_IPHONE

#endif
	}

	public void LogInFB()
	{
#if UNITY_ANDROID
		FacebookAndroid.login ();
#elif UNITY_IPHONE
		
#endif
	}

	void LogInFailed()
	{
		//Application.LoadLevel ("LogIn");
	}
	void LogInSuccess()
	{
		#if UNITY_ANDROID
		FacebookManager.sessionOpenedEvent -= LogInSuccess;
		FacebookManager.loginFailedEvent -= loginFailedEvent;

		var permissions = FacebookAndroid.getSessionPermissions();
		if(permissions.IndexOf("publish_actions")==-1){
			FacebookManager.reauthorizationFailedEvent += reauthorizationFailedEvent;
			FacebookManager.reauthorizationSucceededEvent += reauthorizationSucceededEvent;
			FacebookAndroid.reauthorizeWithPublishPermissions( new string[] { "publish_actions"}, FacebookSessionDefaultAudience.OnlyMe );
			
		}else{
			askForUserData();
		}
		#elif UNITY_IPHONE

		#endif

	}

/*	void loginFailedEvent(P31Error error){
		#if UNITY_ANDROID
		FacebookManager.sessionOpenedEvent -= LogInSuccess;
		FacebookManager.loginFailedEvent -= loginFailedEvent;
#endif
		//TO-DO: check why is not logged in
	}*/
	
	/*void reauthorizationSucceededEvent(){
		FacebookManager.reauthorizationFailedEvent -= reauthorizationFailedEvent;
		FacebookManager.reauthorizationSucceededEvent -= reauthorizationSucceededEvent;
		askForUserData();
	}

	void reauthorizationFailedEvent(P31Error error){
		FacebookManager.reauthorizationFailedEvent -= reauthorizationFailedEvent;
		FacebookManager.reauthorizationSucceededEvent -= reauthorizationSucceededEvent;
		//TO-DO: check why is not giving permission
	}*/

	void askForUserData(){
		GetComponent<UILabel>().text = Language.Get("LOADING")+"...";
		GetComponent<BoxCollider>().enabled = false;
		
		#if UNITY_ANDROID
		Facebook.instance.graphRequest( "me?fields=id,name,email", userDataCompletionHandler );
#endif
	}

	// common event handler used for all Facebook graph requests that logs the data to the console
	void userDataCompletionHandler( string error, object result )
	{
		/*Debug.Log("inside logincompletionhandle");
		if( error != null ){
			Debug.Log("inside error logincompletion handle");
			Debug.LogError( error );
			askForUserData();
		}
		else
		{
			pd = Json.decode<PlayerData> (result.ToString());
			pd.name = pd.name.Replace(' ', '_');//Replace spaces
			pd.id="0";
			Prime31.Utils.logObject("id: "+ pd.id);
			Prime31.Utils.logObject("name: "+ pd.name);
			Prime31.Utils.logObject("email: "+ pd.email);
			StartCoroutine(IdCheck());
		}*/
	}

	IEnumerator UpdateGCMID()
	{
#if UNITY_ANDROID
		string gcmId = GCM.GetRegistrationId();
#elif UNITY_IPHONE
		string gcmId = "";
#endif
		string url = Constants.URL+"setGCMId.php?id="+pd.id+"&gcmId="+gcmId;
		WWW download = new WWW(url);

		
		// Wait until the download is done
		yield return download;
		
		if(download.error != null) {
			print( "Error downloading: " + download.error );
			yield return null;
		} else {
			
			Debug.Log("GCM registration ID set.");
		}
	}
	IEnumerator IdCheck()
	{
		Debug.Log("inside idcheck");
		string post_url =  Constants.URL+"getUserPersonalData.php?id=" + pd.id;
		// Post the URL to the site and create a download object to get the result.
		
		WWW hs_post = new WWW(post_url);
		Debug.Log(post_url);
		yield return hs_post; // Wait until the download is done
		
		if (hs_post.error != null)
		{   
			Debug.Log(hs_post.error);
			LogInFailed();
			//TODO Programar que pasa sin falla conexion
		}
		else
		{
			Debug.Log(hs_post.text);
/*			List<PlayerData> data = Json.decode<List<PlayerData>>(hs_post.text);
			Debug.Log(data.ToString());
			Debug.Log("ammount of same id: " +data.Count.ToString());
			if(data.Count > 0)
			{
				//LoadSaveData.Notifications = data[0].notification;
				Debug.Log("Existing ID");
				StartCoroutine(UpdateGCMID());
				#if UNITY_ANDROID
				if(FacebookAndroid.isSessionValid())
					LoadMenu();
				else
					LogInFailed ();
				#elif UNITY_IPHONE
				#endif
			}
			else
			{
				Debug.Log("Creating User");
					
				string url = Constants.URL+"setNewUser.php?id="+pd.id+"&name="+pd.name+"&email="+pd.email;
				WWW download = new WWW(url);
				Debug.Log("URL: "+url);
				
				// Wait until the download is done
				yield return download;
				
				if(download.error != null) {
					print( "Error downloading: " + download.error );
					yield return null;
				} else {
					StartCoroutine(UpdateGCMID());
					StartCoroutine(UpdateLanguage(Language.CurrentLanguage()));
					#if UNITY_ANDROID
					if(FacebookAndroid.isSessionValid())
						LoadMenu();
					else
						LogInFailed ();
					#elif UNITY_IPHONE
					#endif
					//Debug.Log(download.text);
				}
			}*/
			
		}
	}

	IEnumerator UpdateLanguage(LanguageCode l)
	{
		string str = "EN";
		switch (l)
		{
		case LanguageCode.EU:
			str = "EU";
			break;
		case LanguageCode.ES:
			str = "ES";
			break;
		}
		string url = Constants.URL+"updateUserData.php?id="+PlayerPrefs.GetInt("playerId")+"&field=Language&data="+str;
		WWW download = new WWW(url);
		Debug.Log("URL: "+url);
		
		// Wait until the download is done
		yield return download;
		
		if(download.error != null) {
			print( "Error downloading: " + download.error );
			yield return null;
		} else {
			
			Debug.Log("Language Updated.");
		}
	}

	void LoadMenu()
	{
		Application.LoadLevel ("menu");
		GetComponent<UILabel>().text = Language.Get("LOADING")+"...";
		GetComponent<BoxCollider>().enabled = false;
	}
}
