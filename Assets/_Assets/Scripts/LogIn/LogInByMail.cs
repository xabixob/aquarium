﻿using UnityEngine;
using System.Collections;
using SimpleJSON;

public class LogInByMail : MonoBehaviour {

	public UIInput passw,usua;
	bool isDebug =false;
	void OnClick () {
		if (passw.value!="" && usua.value!=""){
			StartCoroutine("EntrarUsuMail");
		}
		else{
			Debug.Log("Los campos no pueden estar vacios");
		}
	}
	
	public IEnumerator EntrarUsuMail(){
	#if UNITY_ANDROID
		string gcmId = GCM.GetRegistrationId();
	#elif UNITY_IPHONE
		string gcmId = "";
	#endif

		string url = Constants.URL + "loginbymail.php?usu=" + usua.value + "&pass=" + passw.value;
		WWWForm info = new WWWForm ();
		info.AddField ("gcmCode", gcmId);
		WWW download = new WWW (url, info);

	
		yield return download;
	
		if (download.error != null) {
			LoadNewWarningMessage("WARNIN_1");
			yield return null;
		} else {
			JSONNode tmpresp = SimpleJSON.JSONArray.Parse (download.text);
			if (int.Parse (tmpresp ["resp"]) == 1) {
				PlayerPrefs.SetInt ("playerId", tmpresp ["id"].AsInt);
				Application.LoadLevel ("menu");
			} else {
				LoadNewWarningMessage("WARNIN_2");
			}

		}


	}

	private void LoadNewWarningMessage(string mess){
		GameObject.Find ("UI Root").transform.FindChild ("ErrorMessage/Panel/Messaje").GetComponent<UILabel> ().text = Language.Get (mess);
		GameObject.Find ("UI Root").transform.FindChild ("ErrorMessage").gameObject.SetActive (true);
		GameObject.Find ("UI Root").transform.FindChild ("ErrorMessage/Panel").GetComponent<TweenScale> ().PlayForward ();
	}
}
