﻿using UnityEngine;
using System.Collections;

public class ChangeLangLogIn : MonoBehaviour {
	
	public static int languageIndex = 0;
	LanguageCode [] languages = {LanguageCode.EU, LanguageCode.ES, LanguageCode.EN, LanguageCode.FR};
	public UILabel text;
	public GameObject languageEs,languageEu,languageFr,languageEn;
	public UILabel faceLabel, labelUsu, labelPass, labelEnter, labelSkip,labelRegistrar;
	UISprite sprite;
	void Start()
	{
		switch (Language.CurrentLanguage ()) {
		case LanguageCode.EU:
			languageIndex = 0;
			break;
		case LanguageCode.EN:
			languageIndex = 2;
			break;
		case LanguageCode.ES:
			languageIndex = 1;
			break;
		case LanguageCode.FR:
			languageIndex = 3;
			break;
		}


		sprite = GetComponent<UISprite>();
		SetLanguageToCurrent();
		UpdateLanguage ();
	}
	
	void OnClick()
	{
		languageIndex++;
		languageIndex = languageIndex % 4;
		Language.SwitchLanguage(languages[languageIndex]);
		SetLanguageToCurrent();

	}


	void SetLanguageToCurrent()
	{
		switch(Language.CurrentLanguage())
		{
		case LanguageCode.EU:
		{
			languageFr.SetActive(false);
			languageEs.SetActive(false);
			languageEn.SetActive(false);
			languageEu.SetActive(true);

			break;
		}
		case LanguageCode.EN:
		{
			languageEs.SetActive(false);
			languageFr.SetActive(false);
			languageEu.SetActive(false);
			languageEn.SetActive(true);

			break;
		}
		case LanguageCode.ES:
		{
			languageEu.SetActive(false);
			languageFr.SetActive(false);
			languageEn.SetActive(false);
			languageEs.SetActive(true);
			break;
		}
		case LanguageCode.FR:
		{
			languageEn.SetActive(false);
			languageEs.SetActive(false);
			languageEu.SetActive(false);
			languageFr.SetActive(true);


			break;
		}
		}
		UpdateLanguage ();
	}
	public void UpdateLanguage(){
		faceLabel.text = Language.Get("LOG_IN_FB");

		labelUsu.text = Language.Get("USUARIO");
		labelPass.text = Language.Get("PASS");
		labelEnter.text = Language.Get("ENTER");
		labelSkip.text = Language.Get("SKIP");
		labelRegistrar.text = Language.Get("REGISTRARSE");
	}
}
