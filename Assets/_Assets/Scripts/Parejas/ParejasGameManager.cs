﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DarkTonic.MasterAudio;


public class ParejasGameManager : MonoBehaviour {

	public List<GameObject> listaObjetos = new List<GameObject>();
	List<string> listaNombres = new List<string>();
	public int numberOfSelectedObjects=0;
	string LastName;
	int numAciertos=0;
	int aciertosTotales;
	GameObject gameOver;
	public GameObject backButton;

	GameObject pauseMenu;
	public bool IsPaused = false;
	AchievementManager am;

	bool IsGameOver = true;
	GameObject timer;
	float t = 0.0f;
	const int EASY = 4;
	const int MEDIUM = 8;
	const int HARD = 12;
	int difficulty = EASY;
	bool atOnce = true;
	float bestTime;
	string[] pairArrayNames = {"Acervularia","Amonite","Barycrinus","Belemnite","BivalvoSP","Diente","Elegans","Encope","Limul","Mecochirus","Pez","Saukianda","Trilobite"};

	GameObject GrupoObjetos;

	//variables GameEnd

	public UILabel puntosLabel;
	public GameObject labelRanking;
	// Use this for initialization
	void Start ()
	{
		am = GameObject.Find("AchievementsManager").GetComponent<AchievementManager>();
		GrupoObjetos = GameObject.Find("GrupoObjetos");
		GrupoObjetos.SetActive(false);
		gameOver = GameObject.Find ("GameOver");
		pauseMenu = GameObject.Find("Pause");

		timer = GameObject.Find ("Time");
		atOnce = true;


	}

	public void SetTable(string diff)
	{
		GrupoObjetos.SetActive(true);
		difficulty = EASY;
		bestTime = (float)(am.tmpValuesArray ["bestpf"].AsInt / 100f);

		if(diff == "MEDIUM")
		{
			difficulty = MEDIUM;
			bestTime = (float) (am.tmpValuesArray["bestpm"].AsInt/100f);

		}
		else if(diff == "HARD")
		{
			difficulty = HARD;
			bestTime = (float) (am.tmpValuesArray["bestpd"].AsInt/100f);

		}

		int[] randoNumberArray = {0,1,2,3,4,5,6,7,8,9,10,11,12};

		randoNumberArray = ShuffleArray (randoNumberArray);



		listaNombres.Add(pairArrayNames[randoNumberArray[0]]+"_A");
		listaNombres.Add(pairArrayNames[randoNumberArray[0]]+"_B");
		listaNombres.Add(pairArrayNames[randoNumberArray[1]]+"_A");
		listaNombres.Add(pairArrayNames[randoNumberArray[1]]+"_B");
		if(difficulty >= MEDIUM)
		{
			listaNombres.Add(pairArrayNames[randoNumberArray[2]]+"_A");
			listaNombres.Add(pairArrayNames[randoNumberArray[2]]+"_B");
			listaNombres.Add(pairArrayNames[randoNumberArray[3]]+"_A");
			listaNombres.Add(pairArrayNames[randoNumberArray[3]]+"_B");
			if(difficulty >= HARD)
			{
				listaNombres.Add(pairArrayNames[randoNumberArray[4]]+"_A");
				listaNombres.Add(pairArrayNames[randoNumberArray[4]]+"_B");
				listaNombres.Add(pairArrayNames[randoNumberArray[5]]+"_A");
				listaNombres.Add(pairArrayNames[randoNumberArray[5]]+"_B");
			}
		}
		

		for(int i = 0; i < listaObjetos.Count; ++i)
		{
			listaObjetos[i].gameObject.SetActive(true);
			if(i >= difficulty)
			{
				listaObjetos[i].gameObject.SetActive(false);
			}
		}
		listaObjetos.RemoveRange(difficulty, listaObjetos.Count-difficulty);	
		PrintTime ();
		backButton.SetActive (false);
		AsignarImagenesRandom ();
		aciertosTotales = listaObjetos.Count/2;
	}

	//Function will be called whenever the tween at Start object ends
	public void PrintTime()
	{
		//Print best time
		string onena = "- -";
		if(bestTime > 0)
			onena = bestTime.ToString("F2");
		GameObject bt = GameObject.Find("BestTime");
		bt.GetComponent<UILabel>().text = /*Language.Get("BEST_TIME")+"\n" +*/ onena;
		bt.GetComponentInChildren<UISprite>().enabled = true;

		GameObject.Find("TimeLogo").GetComponent<UISprite>().enabled = true;
		//Print time (0.00)
		timer.GetComponent<UILabel>().text = t.ToString("F2");
		timer.GetComponentInChildren<UISprite>().enabled = true;

		GameObject btt = GameObject.Find("BestTimeText");
		btt.GetComponent<UILabel>().enabled = true;
		btt.GetComponentInChildren<UISprite>().enabled = true;
	}

	// Update is called once per frame
	void Update () {
		if(!IsGameOver)
		{
			t = t + Time.deltaTime;
			timer.GetComponent<UILabel>().text = t.ToString("F2");
		}

		//Pause on/off input (back button)
		if(Input.GetKeyDown(KeyCode.Escape))
			IsPaused = togglePause();

	}

	public bool togglePause()
	{
		pauseMenu.GetComponent<UITweener>().PlayReverse();
		//If already paused -> back to play mode
		if(Time.timeScale == 0f)
		{
			Time.timeScale = 1f;
			return false;
		}
		else if(!IsGameOver)//If not paused, pause game
		{
			pauseMenu.GetComponent<UITweener>().PlayForward();
			Time.timeScale = 0f;
			return true;    
		}
		return false;
	}

	private void AsignarImagenesRandom(){

		foreach (GameObject ob in listaObjetos) {
			int randomN = Random.Range(0,listaNombres.Count);
			ob.transform.GetChild(0).GetComponent<UISprite>().spriteName=listaNombres[randomN];
			ob.transform.GetComponent<ObjectManager>().spriteName =listaNombres[randomN];
			listaNombres.RemoveAt(randomN);
		}
	}

	public void ObjectSelected(string name){
		IsGameOver = false;
		numberOfSelectedObjects += 1;

	 if (numberOfSelectedObjects == 2) 
		{
			//check parejas

			if(name.Substring(0,name.Length-1)==LastName)
			{
				numberOfSelectedObjects=numberOfSelectedObjects-2;
				//set both unselectable
				foreach (GameObject ob in listaObjetos) {
					if(ob.GetComponent<ObjectManager>().selected)
					{
						ob.GetComponent<ObjectManager>().selected=false;
						ob.GetComponent<ObjectManager>().SetObjectUnselectable();
					}
				}
				MasterAudio.PlaySound("pairOk");
				CheckGameOver();	
		

			}
			else
			{
				MasterAudio.PlaySound("selected");

				foreach (GameObject ob in listaObjetos) 
				{
					if(ob.GetComponent<ObjectManager>().selected)
						StartCoroutine(taparTrasSecs(ob));
				}
			}
			//Reset check variables
			LastName="";
		}
		else
		{
			MasterAudio.PlaySound("selected");

			LastName = name.Substring(0,name.Length-1);
		}
	}

	public void TaparRapido()
	{
		atOnce = false;
		foreach (GameObject ob in listaObjetos) 
		{
			if(ob.GetComponent<ObjectManager>().selected)
			{
				//ob.GetComponent<ObjectManager>().selected=false;NO POINT ON DOING THAT VEIL FUNCTION DOES IT
				ob.GetComponent<ObjectManager>().Veil();
			}
		}

	}

	IEnumerator taparTrasSecs(GameObject ob)
	{
		atOnce = false;
		yield return new WaitForSeconds(1.5f);
		//RUIDO DESELECT
		ob.GetComponent<ObjectManager>().Veil();
		MasterAudio.PlaySound("error");
			
	}

	public void ObjectDeselected()
	{
		numberOfSelectedObjects -= 1;
	}


	private void CheckGameOver()
	{

		numAciertos+=1;

		if (numAciertos == aciertosTotales) 
		{
			gameOver.GetComponent<UITweener>().PlayForward();
			IsGameOver = true;
			puntosLabel.text = t.ToString("F2");
			if(atOnce)
			{
				if(difficulty == EASY)
				{
					am.achivementsArray ["4AtOnce"].AsInt =1;
				}
				else if(difficulty == MEDIUM)
				{
					am.achivementsArray ["8AtOnce"].AsInt =1;
				}
			}

			if(bestTime == 0 || (bestTime > t && bestTime > 0))
			{

				//LORPENAK************************************
				if(difficulty == EASY)
				{
					am.tmpValuesArray ["bestpf"].AsInt = (int)(t*100);
					am.UpdateBestScore(2,am.tmpValuesArray ["bestpf"].AsInt);
					labelRanking.SetActive(true);
					if(t < 15f)
					{
						am.achivementsArray ["4less15"].AsInt =1;
					}
					if(t < 10f)
					{
						am.achivementsArray ["4less10"].AsInt =1;
					}
					if(t < 5f)
					{
						am.achivementsArray ["4less5"].AsInt =1;
					}
				}
				else if(difficulty == MEDIUM)
				{
					am.tmpValuesArray ["bestpm"].AsInt = (int)(t*100);
					am.UpdateBestScore(3,am.tmpValuesArray ["bestpm"].AsInt);
					labelRanking.SetActive(true);
					if(t < 15f)
					{
						am.achivementsArray ["8less15"].AsInt =1;
					}
					if(t < 10f)
					{
						am.achivementsArray ["8less10"].AsInt =1;
					}
					if(t < 5f)
					{
						am.achivementsArray ["8less5"].AsInt =1;
					}
				}
				else if(difficulty == HARD)
				{
					am.tmpValuesArray ["bestpd"].AsInt = (int)(t*100);
					am.UpdateBestScore(2,am.tmpValuesArray ["bestpd"].AsInt);
					labelRanking.SetActive(true);
					if(t < 15f)
					{
						am.achivementsArray ["12less15"].AsInt =1;
					}
					if(t < 10f)
					{
						am.achivementsArray ["12less10"].AsInt =1;
					}

				}

			}

			am.LoadDataToPlayerPrefs ();
			am.LoadDataToDataBase ();
		}

	}

	int[] ShuffleArray(int[] array){
		System.Random r = new System.Random();
		for (int i = array.Length; i > 0; i--)
		{
			int j = r.Next(i);
			int k = array[j];
			array[j] = array[i - 1];
			array[i - 1]  = k;
		}
		return array;
	}



}
