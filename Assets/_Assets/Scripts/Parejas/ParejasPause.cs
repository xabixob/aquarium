﻿using UnityEngine;
using System.Collections;

public class ParejasPause : MonoBehaviour {

	ParejasGameManager manager;

	void Start()
	{
		manager = GameObject.Find ("Manager").GetComponent<ParejasGameManager>();
	}
	

	public void OnClick()
	{
		manager.IsPaused = manager.togglePause();
	}
}
