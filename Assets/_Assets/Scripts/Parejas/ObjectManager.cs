﻿using UnityEngine;
using System.Collections;

public class ObjectManager : MonoBehaviour {

	public bool selected=false;
	int parejaNumber = 0;
	GameObject frontSprite;
	ParejasGameManager parejasManagerS;
	public string spriteName;
	
	// Use this for initialization
	void Start () {
		frontSprite = transform.GetChild (1).gameObject;
		parejasManagerS = GameObject.Find ("Manager").GetComponent<ParejasGameManager> ();
	}


	public void OnClick(){

		if(parejasManagerS.IsPaused)
			return;
		if (!selected) {
			if(parejasManagerS.numberOfSelectedObjects>=2)
			{
				parejasManagerS.TaparRapido();
			}
			Unveil();
		}
		else
		{
		//	StopAllCoroutines();
			//StopCoroutine (parejasManagerS.coroutine1);
			//StopCoroutine (parejasManagerS.coroutine2);
			Veil ();
		}
	}

	public void SetParejaNumber(int n){
		parejaNumber = n;
	}

	public int GetParejaNumber(){

		return parejaNumber;
	}

	private  void Unveil()
	{
		selected = true;
		frontSprite.GetComponent<UISprite> ().alpha = 0;
		parejasManagerS.ObjectSelected(spriteName);
	}

	public void Veil(){
		if(selected)
		{
			selected = false;
			parejasManagerS.ObjectDeselected();

			frontSprite.GetComponent<UISprite> ().alpha = 1;
		}
	}

	public void SetObjectUnselectable(){
		GetComponent<BoxCollider>().size = new Vector3(0,0,0);
	}

	
}
