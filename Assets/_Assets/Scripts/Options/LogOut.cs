﻿using UnityEngine;
using System.Collections;
//using Prime31;
public class LogOut : MonoBehaviour 
{
	public void OnClick()
	{
		Debug.Log("LOG OUT!");
		PlayerPrefs.DeleteAll ();
		Destroy (GameObject.Find ("AchievementsManager"));
		#if UNITY_ANDROID
		FacebookAndroid.logout();
		#elif UNITY_IPHONE
		#endif

		Debug.Log("LOG OUT!");
		Application.LoadLevel ("LogIn");
	}
}
