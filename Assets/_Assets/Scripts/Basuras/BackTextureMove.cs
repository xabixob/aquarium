﻿using UnityEngine;
using System.Collections;

public class BackTextureMove : MonoBehaviour {

	UITexture tex;
	public float moveSpeed;
	// Use this for initialization
	void Start () {

		tex = GetComponent<UITexture>();
	}
	
	// Update is called once per frame
	void Update () {

		if (tex != null) {
			Rect rect = tex.uvRect;
			rect.x -= Time.deltaTime * moveSpeed;
			//	rect.y -= offsetY * Time.deltaTime*moveSpeed;
			if (rect.x > 1 || rect.x < -1) {
				rect.x = rect.x - (int)rect.x;
			}	
			/*if(rect.y>1||rect.y<-1){
				rect.y = rect.y- (int)rect.y;
			}*/
			
			tex.uvRect = rect;
		} else {
			transform.localPosition = new Vector3(transform.localPosition.x+moveSpeed,transform.localPosition.y,transform.localPosition.z);
			if (transform.localPosition.x+moveSpeed>1365){
				transform.localPosition = new Vector3(-1486,transform.localPosition.y,transform.localPosition.z);
			}
		}
	}
}
