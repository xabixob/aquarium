﻿using UnityEngine;
using System.Collections;

public class moveBullet : MonoBehaviour {


	public float speedZ;
	public float speedY;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		float x = transform.localPosition.x;
		float y = transform.localPosition.y;
		float z = transform.localPosition.z;
		transform.localPosition = new Vector3 (x, y + speedY * Time.deltaTime, z + speedZ * Time.deltaTime);
	}
}
