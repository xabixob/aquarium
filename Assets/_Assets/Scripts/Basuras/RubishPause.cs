﻿using UnityEngine;
using System.Collections;

public class RubishPause : MonoBehaviour {

	GameManager manager;
	
	void Start()
	{
		manager = GameObject.Find ("GameManager").GetComponent<GameManager>();
	}
	
	
	public void OnClick()
	{
		manager.IsPaused = manager.togglePause();
	}
}
