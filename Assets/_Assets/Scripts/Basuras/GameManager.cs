﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

	GameObject StartUI;


	//GameObject t;
	GameObject Reloading;
	GameObject GameOver;
	GameObject Cam;
	AchievementManager am;
	 

	bool CanPause = false;
	public bool IsPaused = false;
	GameObject pauseMenu;
	public int specials;
	public int totalspecials;
	public int hit;
	public int shot;

	//variables del gameOver

	public UILabel labelPuntos;
	public GameObject labelNewRecord;
	void Start(){

		specials = 0; 
		hit = 0;
		shot= 0;
		am = GameObject.Find("AchievementsManager").GetComponent<AchievementManager>();
		Screen.orientation = ScreenOrientation.LandscapeLeft;
		StartUI = GameObject.Find ("GameStart");

		//t = GameObject.Find ("Time");
		Reloading = GameObject.Find ("Reloading");
		GameOver = GameObject.Find ("GameOver");
		Cam = GameObject.Find ("Camera");

		pauseMenu = GameObject.Find("Pause");

	}
	public void StartGameOver(){
		CanPause = false;

		labelPuntos.text = GetComponent<ScoreManager> ().score + "";
		if (GetComponent<ScoreManager> ().score > am.tmpValuesArray ["bestkr"].AsInt) {
			am.tmpValuesArray ["bestkr"].AsInt = GetComponent<ScoreManager> ().score;
			am.UpdateBestScore(1,GetComponent<ScoreManager> ().score);
			labelNewRecord.SetActive(true);
		}
		am.tmpValuesArray ["tirosHechos"].AsInt = am.tmpValuesArray ["tirosHechos"].AsInt + shot;
		am.tmpValuesArray ["tirosFallados"].AsInt = am.tmpValuesArray ["tirosFallados"].AsInt + (shot-hit);
		am.tmpValuesArray ["basuraDestruida"].AsInt = am.tmpValuesArray ["basuraDestruida"].AsInt + (hit);
		am.tmpValuesArray ["numEspecialesTotal"].AsInt = am.tmpValuesArray ["numEspecialesTotal"].AsInt + (specials);



		if ((GetComponent<ScoreManager>().score > 100) && (am.achivementsArray ["g100p"].AsInt < 0)) {
			am.achivementsArray ["g100p"].AsInt =1;
		}
		if ((specials == GetComponent<SpawnTrash>().specials) && (am.achivementsArray ["dAllSp"].AsInt < 0)) {
			am.achivementsArray ["dAllSp"].AsInt =1;
		}
		if ((am.tmpValuesArray ["numEspecialesTotal"].AsInt>30) && (am.achivementsArray ["d30Sp"].AsInt < 0)) {
			am.achivementsArray ["d30Sp"].AsInt =1;
		}
		if ((am.tmpValuesArray ["numEspecialesTotal"].AsInt>100) && (am.achivementsArray ["d100Sp"].AsInt < 0)) {
			am.achivementsArray ["d100Sp"].AsInt =1;
		}
		if ((am.tmpValuesArray ["tirosFallados"].AsInt>500) && (am.achivementsArray ["m500sh"].AsInt < 0)) {
			am.achivementsArray ["m500sh"].AsInt =1;
		}
		if ((am.tmpValuesArray ["basuraDestruida"].AsInt>50) && (am.achivementsArray ["d50Ru"].AsInt < 0)) {
			am.achivementsArray ["d50Ru"].AsInt =1;
		}
		if ((am.tmpValuesArray ["basuraDestruida"].AsInt>150) && (am.achivementsArray ["d150Ru"].AsInt < 0)) {
			am.achivementsArray ["d150Ru"].AsInt =1;
		}
		if ((am.tmpValuesArray ["tirosHechos"].AsInt>150) && (am.achivementsArray ["s500B"].AsInt < 0)) {
			am.achivementsArray ["s500B"].AsInt =1;
		}
		if ((GetComponent<ScoreManager>().score > 100) &&(((hit+specials)*100/shot)>50) && (am.achivementsArray ["50of80"].AsInt < 0)) {
			am.achivementsArray ["50of80"].AsInt =1;
		}
		if ((GetComponent<ScoreManager>().score > 100) &&(((hit+specials)*100/shot)>75) && (am.achivementsArray ["75of80"].AsInt < 0)) {
			am.achivementsArray ["75of80"].AsInt =1;
		}
		if ((GetComponent<ScoreManager>().score > 100) &&(((hit+specials)*100/shot)>50) && (am.achivementsArray ["100of80"].AsInt < 0)) {
			am.achivementsArray ["100of80"].AsInt =1;
		}

		am.LoadDataToPlayerPrefs ();
		am.LoadDataToDataBase ();

		Cam.GetComponent<shootBall> ().canPlay = false;
	//	t.GetComponent<UITweener> ().PlayForward ();
		Reloading.GetComponent<UITweener> ().PlayForward ();
		GameOver.GetComponent<UITweener> ().PlayForward ();
		GameObject.Find ("BulletManager").GetComponent<PoolScript> ().disableAll ();

	}

	public void StartGame(){

		StartUI.transform.localScale = Vector3.zero;
		Reloading.transform.localScale = Vector3.one;
		Cam.GetComponent<shootBall> ().canPlay = true;
		GetComponent<TimeManager> ().startCounting = true;
		TweenAlpha [] tw = GameObject.Find("GameInfo").GetComponentsInChildren<TweenAlpha>(true);
		foreach(TweenAlpha alphaTW in tw)
		{
			alphaTW.PlayForward();
		}
		CanPause = true;

	}

	void Update () 
	{
		//Pause on/off input (back button)
		if(Input.GetKeyDown(KeyCode.Escape))
			IsPaused = togglePause();
	}

	public bool togglePause()
	{
		pauseMenu.GetComponent<UITweener>().PlayReverse();
		//If already paused -> back to play mode
		if(Time.timeScale == 0f)
		{
			StartCoroutine(CanPlayDelayed());
			Time.timeScale = 1f;
			return false;
		}
		else if(CanPause)//If not paused, pause game
		{
			Cam.GetComponent<shootBall> ().canPlay = false;
			pauseMenu.GetComponent<UITweener>().PlayForward();
			Time.timeScale = 0f;
			return true;    
		}
		return false;
	}

	IEnumerator CanPlayDelayed()
	{
		yield return new WaitForSeconds(0.5f);
		Cam.GetComponent<shootBall> ().canPlay = true;
	}
}
