﻿using UnityEngine;
using System.Collections;
using DarkTonic.MasterAudio;

public class CheckCollision : MonoBehaviour {

	GameManager gm;

	void Start(){
		gm = GameObject.Find ("GameManager").GetComponent<GameManager> (); 
	}


	void OnTriggerEnter(Collider other) {
		//Debug.Log (other.gameObject);
		if (other.gameObject.tag == "Trash") {
			other.gameObject.GetComponentInParent<die>().Explode();
			MasterAudio.PlaySound("ExplosionShort");
			if (other.transform.GetChild(0).GetComponent<UISprite>().spriteName.Equals("bullet")){
				++gm.specials;
			}
			else{
				++gm.hit;
			}
			this.gameObject.SetActive(false);
		}




	}

}
