﻿using UnityEngine;
using System.Collections;
using DarkTonic.MasterAudio;

public class shootBall : MonoBehaviour {

	public GameObject ball;
	public float speed = 50;


	public bool canPlay=false;
 	public bool canShoot=false;

	public float reloadingTime=0.5f;
	float reloadTimeCounter=0;
	public UISlider reloadSlider;
	UILabel reloadTextLabel;

	//RaycastHit hit;
	GameObject trabuco;
	float ratio;
	float ancho;

	GameManager gm;


	void Start () {
		gm = GameObject.Find("GameManager").GetComponent<GameManager>();

		trabuco = GameObject.Find("Trabuco");

		ratio = Screen.height/1024f;
		ancho = Screen.width/ratio;

		NGUITools.SetActive (trabuco, false);
		reloadTextLabel = reloadSlider.GetComponentInChildren<UILabel> ();
		//hit = new RaycastHit(); 


		reloadTextLabel.text = Language.Get("LOADING");
		reloadSlider.value = 0;
		MasterAudio.PlaySound("OceanWaves"); 
		canShoot=false;
	}

	void FixedUpdate(){

		if (!canShoot&&canPlay) {
			reloadSlider.value = (reloadTimeCounter / reloadingTime);
			reloadTimeCounter+=Time.deltaTime;
			reloadTextLabel.text = Language.Get("LOADING");

			if(reloadTimeCounter>reloadingTime){
				canShoot=true;
				reloadTimeCounter=0;
				reloadTextLabel.text = Language.Get("SHOOT");
			}
		}
	}


	void Update(){

		if(Input.GetMouseButtonUp(0)&&canShoot&&canPlay){
			canShoot=false;
			Ray ray = this.GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);

			SetTrabuco();
		
			//if(Physics.Raycast(ray, out hit, 300.0f))
			{
				GameObject newBall = GameObject.FindWithTag("BulletManager").GetComponent<PoolScript>().GetPooledObject();
				newBall.transform.position=transform.position;
				newBall.transform.rotation=transform.rotation;
				newBall.SetActive(true);
				newBall.GetComponent<Renderer>().enabled=false;
				StartCoroutine(showLastBall(newBall));
				newBall.GetComponent<die>().launchDie();
				newBall.GetComponent<Rigidbody>().velocity = (/*hit.point - transform.position*/ray.direction).normalized * speed;
				MasterAudio.PlaySound("Shoot"); 

				++gm.shot;


			}
		}
	}

	IEnumerator showLastBall(GameObject ball){
		yield return new WaitForSeconds(0.5f);
		ball.GetComponent<Renderer> ().enabled=true;
		
	}

	void setPosTrabucoSprite(){

//		float alto = GameObject.Find ("Game").GetComponent<UIRoot>().manualHeight / 2;
		float ancho = Screen.width/ratio;
		trabuco.transform.GetChild (0).localPosition = new Vector3(ancho, 0, 250);


	}
	void SetTrabuco(){
		//trabuco

		ratio = Screen.height/1024f;
		NGUITools.SetActive (trabuco, true);
		trabuco.transform.localPosition= new Vector3(((Input.mousePosition.x-Screen.width/2f)/ratio)/(850f/200f),0,0);
		StartCoroutine(animTrabuco());


	}


	IEnumerator animTrabuco(){
		UISpriteAnimation anim = trabuco.GetComponentInChildren<UISpriteAnimation>();
		anim.Play();
		while (anim.isPlaying) {
			yield return 0;		
		}
		NGUITools.SetActive (trabuco, false);
	}
	
}