﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PoolScriptNGUI : MonoBehaviour {
	


	public GameObject pooledObject;
	public int pooledAmount = 20;
	public bool willGrow = true;
	
	
	public List<GameObject> pooledObjects;
	
	void Awake ()
	{

		pooledObjects = new List<GameObject>();
		for(int i = 0; i < pooledAmount; i++)
		{
			GameObject obj  = NGUITools.AddChild (this.gameObject,pooledObject);
		//	obj.transform.localScale.Set = new Vector3(1,1,1);
			obj.transform.parent=this.gameObject.transform;
			obj.SetActive(false);
			pooledObjects.Add(obj);
		}
	}
	
	public GameObject GetPooledObject()
	{
		for(int i = 0; i< pooledObjects.Count; i++)
		{
		
			if(!pooledObjects[i].activeInHierarchy)
			{
				return pooledObjects[i];
			}    
		}
		
		if (willGrow)
		{
			GameObject obj = (GameObject)NGUITools.AddChild (this.gameObject,pooledObject);
			pooledObjects.Add(obj);
			return obj;
		}
		
		return null;
	}
	
}