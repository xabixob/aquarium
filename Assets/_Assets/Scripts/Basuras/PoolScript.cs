﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PoolScript : MonoBehaviour {
	


	public GameObject pooledObject;
	public int pooledAmount = 20;
	
	
	public List<GameObject> pooledObjects;
	
	void Awake ()
	{

		pooledObjects = new List<GameObject>();
		for(int i = 0; i < pooledAmount; i++)
		{
			GameObject obj  = (GameObject)Instantiate(pooledObject);// NGUITools.AddChild (this.gameObject,pooledObject);
			obj.transform.parent=this.gameObject.transform;
			obj.SetActive(false);
			pooledObjects.Add(obj);
		}
	}
	
	public GameObject GetPooledObject()
	{
		for(int i = 0; i< pooledObjects.Count; i++)
		{
			if(!pooledObjects[i].activeInHierarchy)
			{
				return pooledObjects[i];
			}    
		}

		GameObject obj = (GameObject)Instantiate(pooledObject);
		pooledObjects.Add(obj);
		return obj;
	}

	public void disableAll(){

		for(int i = 0; i< pooledObjects.Count; i++)
		{	
			pooledObjects[i].SetActive(false);
		}
	}
	
}