﻿using UnityEngine;
using System.Collections;

public class ScoreManager : MonoBehaviour {

	public UILabel scoreLabel;
	public int score=0;
	public UILabel maxScoreLabel;
	private int max;
	AchievementManager am;
	public float moveSpeed = 100f;

	// Use this for initialization
	void Start () {
		am = GameObject.Find("AchievementsManager").GetComponent<AchievementManager>();
		scoreLabel.text = score.ToString ()+ " "+ Language.Get("POINTS");
	//	waves = GameObject.Find("Sea").GetComponentsInChildren<AnimWave>(true);
		moveSpeed = 100f;
		max = am.tmpValuesArray ["bestkr"].AsInt;

		maxScoreLabel.text = Language.Get("HIGHEST_SCORE")+"\n" + max.ToString();


	}

	public void addPoints(int points){
		score += points;
		scoreLabel.text = score.ToString ()+ " "+ Language.Get("POINTS");

	//	foreach(AnimWave w in waves)
	//	{
			moveSpeed += points;
	//	}

		if(score >= 100)
		{

		}
		if(score > max)
		{
			maxScoreLabel.text = Language.Get("HIGHEST_SCORE")+"\n" + score.ToString();
		}
	}
}
