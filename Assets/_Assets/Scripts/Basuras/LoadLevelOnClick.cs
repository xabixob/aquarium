﻿using UnityEngine;
using System.Collections;

public class LoadLevelOnClick : MonoBehaviour {

	public string levelName;
	GameObject fadeObject;

	void Start(){
		fadeObject = GameObject.Find ("FadeImage");
	
	}
	public void OnClick()
	{
		if(levelName == "_Irten_")
		{
			Application.Quit();
		}

		else if (levelName == Application.loadedLevelName) 
		{
			Application.LoadLevel (levelName);
		}
		else 
		{
			//If it is in pause mode, remove for the fade in
			if(0 == Time.timeScale)
				Time.timeScale = 1f;

			StartCoroutine(loadFading());
		}

	}

	IEnumerator loadFading(){
		if(Application.loadedLevelName == "achievements")
		{
			GameObject.Find("Window").SetActive(false);
		}
		fadeObject.GetComponent<fadeManager> ().fadeIn ();
		yield return new WaitForSeconds (1f);
		Application.LoadLevel (levelName);
	}
}
