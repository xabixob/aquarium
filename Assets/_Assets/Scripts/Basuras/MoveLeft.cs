﻿using UnityEngine;
using System.Collections;

public class MoveLeft : MonoBehaviour {

	public float speedOffset = 10f;
	ScoreManager SM;
	// Use this for initialization
	void Start () {
		SM = GameObject.Find("GameManager").GetComponent<ScoreManager>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.localPosition = new Vector3 (transform.localPosition.x - (SM.moveSpeed-speedOffset) * Time.deltaTime, transform.localPosition.y, transform.localPosition.z);
	}
}
