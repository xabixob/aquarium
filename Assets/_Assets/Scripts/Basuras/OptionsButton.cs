﻿using UnityEngine;
using System.Collections;

public class OptionsButton : MonoBehaviour {

	public bool showing = false;
	bool allDone = true;
	public void IsClicked()
	{
		//GetComponent<BoxCollider>().enabled = allDone;
		if(!allDone)
			return;
		OptionsMenu[] Options = GetComponentsInChildren<OptionsMenu>(true);
		if(showing)
		{
			//HIDE THE OPTIONS MENU
			foreach(OptionsMenu o in Options)
			{
				o.Hide();
			}
		}
		else
		{
			//SHOW THE OPTIONS MENU
			foreach(OptionsMenu o in Options)
			{
				o.Show();
			}
		}
		showing = !showing;
		allDone = false;
		GetComponent<SphereCollider>().enabled = allDone;

	}
	

	public void LastButtonFinished () 
	{
		allDone = true;
		GetComponent<SphereCollider>().enabled = allDone;
	}
}
