﻿using UnityEngine;
using System.Collections;

public class TimeManager : MonoBehaviour {


	public int initialTime=2;
	public UILabel timeCounterLabel;
	public bool startCounting=false;

	private float timecounter=0;

	// Use this for initialization
	void Start () {
		timeCounterLabel.text = initialTime.ToString() + " "+ Language.Get("SECONDS");
	}
	
	// Update is called once per frame
	void FixedUpdate () {
	
		if (startCounting) {
			timecounter += Time.deltaTime;
		
			if (timecounter > 1) {
				timecounter -= 1;
				initialTime -= 1;
				timeCounterLabel.text = initialTime.ToString () + " "+ Language.Get("SECONDS");
				if(initialTime == 1)
				{
					if(Language.CurrentLanguage() == LanguageCode.EU)
						timeCounterLabel.text = "segundo" + initialTime.ToString ();
					else
						timeCounterLabel.text = initialTime.ToString () + " "+ Language.Get("SECOND");
				}

				if (initialTime == 0) {
					GameObject.FindWithTag ("GameManager").GetComponent<GameManager> ().StartGameOver ();
					timeCounterLabel.text = "GAME OVER!";
					startCounting = false;

				}
			}
		}
	}
}
