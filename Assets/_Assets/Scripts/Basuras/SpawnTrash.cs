﻿using UnityEngine;
using System.Collections;

public class SpawnTrash : MonoBehaviour {


	//tiempo
	public float minSpawnTime0;
	public float maxSpawnTime0;
	private float timeBetweenSpawns0;
	private float timeCounter0;

	public float minSpawnTime1;
	public float maxSpawnTime1;
	private float timeBetweenSpawns1;
	private float timeCounter1;

	public float minSpawnTime2;
	public float maxSpawnTime2;
	private float timeBetweenSpawns2;
	private float timeCounter2;

	//posicion fila1
	public float fila0Y;
	public float fila0Z;

	//posicion fila2
	public float fila1Y;
	public float fila1Z;

	//posicion fila3
	public float fila2Y;
	public float fila2Z;

	//Script references
	private PoolScriptNGUI trashPoolS;


	public int specials = 0;
	float tSpecial;
	// Use this for initialization


	void Start () {

		timeBetweenSpawns0 = Random.Range (minSpawnTime0, maxSpawnTime0);
		trashPoolS = GameObject.FindWithTag ("TrashManager").GetComponent<PoolScriptNGUI> ();
	}
	
	// Update is called once per frame
	void Update () {

		timeCounter0 += Time.deltaTime;
		timeCounter1 += Time.deltaTime;
		timeCounter2 += Time.deltaTime;

		tSpecial = Random.Range(40f - (specials+1)*7f, 41f - specials * 7f);

		float it = GetComponent<TimeManager>().initialTime;
		if(tSpecial > it && it > 5f && it < 40f )
			SpawnSpecial(Random.Range(0, 3));

		if (timeCounter0 > timeBetweenSpawns0) {
			timeCounter0-=timeBetweenSpawns0;
			timeBetweenSpawns0 = Random.Range (minSpawnTime0, maxSpawnTime0);
			SpawnTrashObject(0);
		}

		if (timeCounter1 > timeBetweenSpawns1) {
			timeCounter1-=timeBetweenSpawns1;
			timeBetweenSpawns1 = Random.Range (minSpawnTime1, maxSpawnTime1);
			SpawnTrashObject(1);
		}

		if (timeCounter2 > timeBetweenSpawns2) {
			timeCounter2-=timeBetweenSpawns2;
			timeBetweenSpawns2 = Random.Range (minSpawnTime2, maxSpawnTime2);
			SpawnTrashObject(2);
		}
	}

	private void SpawnSpecial(int fila){
		
		specials++;
		GameObject obj = trashPoolS.GetPooledObject ();
		obj.SetActive (true);
		obj.GetComponent<MoveLeft>().speedOffset = -50f;
		obj.GetComponent<die>().lifeTime = 10f;
		//SPRITE
		UISpriteAnimation anim = obj.GetComponentInChildren<UISpriteAnimation>();
		anim.Pause();//enabled = false;
		UISprite n = obj.GetComponentInChildren<UISprite>();
		obj.GetComponentInChildren<UISprite> ().width = 200;
		obj.GetComponentInChildren<UISprite> ().height= 200;
		Vector2 size = obj.GetComponentInChildren<UISprite>().localSize;
		obj.GetComponentInChildren<BoxCollider>().size = new Vector3(size.x, size.y, 20f);
		obj.GetComponentInChildren<UISprite>().spriteName="Xtra";
		obj.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
		
		//POSITION
		if (fila == 0) {
			obj.GetComponent<die>().SetPointValue(5);
			obj.GetComponentInChildren<UISprite>().depth=9;
			obj.transform.localPosition = new Vector3 (650f, fila0Y, fila0Z);
			
		} else if (fila == 1) {
			obj.GetComponent<die>().SetPointValue(10);
			obj.GetComponentInChildren<UISprite>().depth=7;
			obj.transform.localPosition = new Vector3 (650f, fila1Y, fila1Z);
			
		} else if (fila == 2) {
			obj.GetComponent<die>().SetPointValue(15);
			obj.GetComponentInChildren<UISprite>().depth=5;
			obj.transform.localPosition = new Vector3 (650f, fila2Y, fila2Z);
			
		}
		
		
		obj.GetComponent<die>().launchDie();
	}

	private void SpawnTrashObject(int fila){


		GameObject obj = trashPoolS.GetPooledObject ();
		obj.SetActive (true);
		obj.GetComponent<MoveLeft>().speedOffset = 10f;

		//SPRITE

		int spriteSelector = 0;

		spriteSelector = Random.Range (0, 3);

		if (spriteSelector == 0) {
			UISpriteAnimation anim = obj.GetComponentInChildren<UISpriteAnimation>();
			anim.Pause();

			obj.GetComponentInChildren<UISprite>().spriteName="Caja_00";
			anim.framesPerSecond = 15;
			anim.namePrefix = "Caja_";
			anim.loop = true;
			anim.Play();
			obj.transform.localScale = /*new Vector3(1f, 1f, 1f);*/new Vector3(0.25f, 0.25f, 0.5f);
		}
		else if (spriteSelector == 1)
		{


			obj.GetComponentInChildren<UISprite>().spriteName="Lata_00";
			UISpriteAnimation anim = obj.GetComponentInChildren<UISpriteAnimation>();
			anim.framesPerSecond = 15;
			anim.namePrefix = "Lata_";
			anim.loop = true;
			anim.Play();
			obj.transform.localScale = /*new Vector3(1f, 1f, 1f);*/new Vector3(0.25f, 0.25f, 0.5f);
		}
		else if(spriteSelector == 2)
		{
			obj.GetComponentInChildren<UISprite>().spriteName="Wheel_00";
			UISpriteAnimation anim = obj.GetComponentInChildren<UISpriteAnimation>();
			anim.framesPerSecond = 15;
			anim.namePrefix = "Wheel_";
			anim.loop = true;
			anim.Play();
			obj.transform.localScale = /*new Vector3(1f, 1f, 1f);*/new Vector3(0.25f, 0.25f, 0.5f);

		}
		UISprite n = obj.GetComponentInChildren<UISprite>();
		n.MakePixelPerfect();
		obj.transform.localScale = new Vector3(0.25f, 0.25f, 0.5f);
		obj.transform.GetChild(0).localScale = new Vector3(0.25f, 0.25f, 0.5f);
		Vector2 size = obj.GetComponentInChildren<UISprite>().localSize;
		obj.GetComponentInChildren<BoxCollider>().size = new Vector3(size.x, size.y, 20f);
	
		//POSITION

		if (fila == 0) {
			obj.GetComponent<die>().SetPointValue(5);
			obj.GetComponentInChildren<UISprite>().depth=9;
			obj.transform.localPosition = new Vector3 (650f, fila0Y, fila0Z);

		} else if (fila == 1) {
			obj.GetComponent<die>().SetPointValue(10);
			obj.GetComponentInChildren<UISprite>().depth=7;
			obj.transform.localPosition = new Vector3 (650f, fila1Y, fila1Z);

		} else if (fila == 2) {
			obj.GetComponent<die>().SetPointValue(15);
			obj.GetComponentInChildren<UISprite>().depth=5;
			obj.transform.localPosition = new Vector3 (650f, fila2Y, fila2Z);

		}


		obj.GetComponent<die>().launchDie();
	}


}
