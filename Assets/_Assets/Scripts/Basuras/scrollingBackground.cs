﻿using UnityEngine;
using System.Collections;

public class scrollingBackground : MonoBehaviour {

	public float factor = 0.0005f;
	public GameObject ship;
	Vector3 shipPosition;
	float offsetX,offsetY;
	
	// Use this for initialization
	void Start () {

		shipPosition = new Vector3(ship.transform.position.x,ship.transform.position.y,0);
		offsetX=0;
		offsetY=0;

	
	}
	
	// Update is called once per frame
	void Update () {
		

		
	}
	
	void FixedUpdate () {

	
		offsetX = ship.transform.position.x*factor;
		offsetY = ship.transform.position.y*factor;
	/*	if (offsetX<-1){offsetX=1;}
		if (offsetY<-1){offsetY=1;}
		
		if (offsetX>1){offsetX=-1;}
		if (offsetY>1){offsetY=-1;}*/
		GetComponent<Renderer>().material.mainTextureOffset = new Vector2 (offsetX,offsetY);

		shipPosition = new Vector3(ship.transform.position.x, ship.transform.position.y,0.1f);
		this.transform.position = shipPosition;
		

	}
}
