﻿using UnityEngine;
using System.Collections;

public class die : MonoBehaviour {

	public float lifeTime;
	public int pointsValue=5;
	PoolScriptNGUI PointFeedback;
	GameManager gm;
	void Start()
	{
		gm = GameObject.Find("GameManager").GetComponent<GameManager>();
		PointFeedback = GameObject.Find("PointFeedbackManager").GetComponent<PoolScriptNGUI>();
	}
	public void launchDie(){

		StartCoroutine(Die ());
	}

	IEnumerator Die(){

		yield return new WaitForSeconds(lifeTime);
		this.gameObject.SetActive(false);
	}

	public void Explode(){

		GameObject obj = PointFeedback.GetPooledObject();
		obj.SetActive(true);
		obj.transform.localPosition = gameObject.transform.localPosition;
		obj.GetComponent<PointFeedback>().ShowPointsFeedback(pointsValue.ToString());

		if (gameObject.transform.childCount > 0) {
			UISpriteAnimation anim = transform.FindChild("Explosion").GetComponent<UISpriteAnimation> ();
			UISpriteAnimation animpre = transform.FindChild("Image").GetComponent<UISpriteAnimation> ();
			anim.Pause();
			anim.loop = false;
			if (animpre.namePrefix.Equals("Caja")){
				transform.FindChild("Explosion").GetComponent<UISprite> ().spriteName = "Maderan_Boom_00";
				anim.namePrefix = "Maderan_Boom_";
			}
			else if (animpre.namePrefix.Equals("Lata_")){
				transform.FindChild("Explosion").GetComponent<UISprite> ().spriteName = "Metal_Boom_00";
				anim.namePrefix = "Metal_Boom_";
			}
			else{
				transform.FindChild("Explosion").GetComponent<UISprite> ().spriteName = "Rueda_Boom_00";
				anim.namePrefix = "Rueda_Boom_";
			}

			transform.FindChild("Explosion").gameObject.SetActive(true);
			transform.FindChild("Image").gameObject.SetActive(false);
			anim.Play ();

			/*if (anim.namePrefix.Equals("Maderan_Boom_")){
				gameObject.GetComponentInChildren<UISprite> ().spriteName = "Maderan_Boom_00";
				
			}
			else if (anim.namePrefix.Equals("Metal_Boom_")){
				gameObject.GetComponentInChildren<UISprite> ().spriteName = "Metal_Boom_00";
			}
			else{
				gameObject.GetComponentInChildren<UISprite> ().spriteName = "Rueda_Boom_00";
			}*/

		}
		//Check if it was special rubbish
		if (GetComponentInChildren<UISprite> ().spriteName == "bullet") {
		}

		StopCoroutine ("Die");
		GameObject.FindWithTag ("GameManager").GetComponent<ScoreManager> ().addPoints (pointsValue);
		if (gameObject.transform.childCount > 0) {
			StartCoroutine("WaitToEndAnimation");
		} else {
			NGUITools.SetActive (this.gameObject, false);
		}
	}

	public IEnumerator WaitToEndAnimation(){
		yield return new WaitForSeconds (1);
		NGUITools.SetActive (this.gameObject, false);
	}
	public void SetPointValue(int p){
		pointsValue = p;
	}
}
