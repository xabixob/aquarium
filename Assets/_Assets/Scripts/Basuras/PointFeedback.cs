﻿using UnityEngine;
using System.Collections;

public class PointFeedback : MonoBehaviour {

	public void ShowPointsFeedback(string points)
	{
		GetComponent<UILabel>().alpha = 1.0f;
		GetComponent<UILabel>().text = "+"+points;
		TweenPosition.Begin(gameObject, 20f, transform.localPosition + new Vector3(0, 500, 0));
		StartCoroutine(Die ());
		TweenAlpha.Begin(gameObject,5f,0f);
	}
	IEnumerator Die(){
		
		yield return new WaitForSeconds(20f);
		this.gameObject.SetActive(false);
	}
}
