﻿using UnityEngine;
using System.Collections;
using SimpleJSON;

public class OnEnviarRegistroClick : MonoBehaviour {
	public UIInput inputUsu,inputPass1,inputPass2,inputMail1,inputMail2;
	public UILabel labelUsu, labelMail1,labelMail2,labelPass1,labelPass2, labelEnviar;

	void Start(){
		labelUsu.text = Language.Get("USUARIO");
		labelMail1.text = Language.Get("MAIL");
		labelMail2.text = Language.Get("REP_MAIL");
		labelPass1.text = Language.Get("PASS");
		labelPass2.text = Language.Get("REP_PASS");
		labelEnviar.text = Language.Get("ENVIAR");
	}
	// Use this for initialization
	void OnClick () {
		if (inputPass1.value==inputPass2.value){
			if (inputMail1.value==inputMail2.value){
				StartCoroutine("RegistrarUsu");
			}
			else{
				Debug.Log("No coinciden los emails");
			}
		}
		else{
			Debug.Log("No coinciden los passwords");
		}
	}

	public IEnumerator RegistrarUsu(){
		#if UNITY_ANDROID
		string gcmId = GCM.GetRegistrationId();
		#elif UNITY_IPHONE
		string gcmId = "";
		#endif
		string url = Constants.URL+"createnewplayer.php?usu="+inputUsu.value+"&pass="+inputPass1.value+"&mail="+inputMail1.value;
		WWW download = new WWW(url);
		Debug.Log("URL: "+url);
		
		yield return download;
		
		if(download.error != null) {
			Debug.Log("Se ha producido un error en el registro, intentelo mas tarde");
			yield return null;
		} else {
			JSONNode tmpresp = SimpleJSON.JSONArray.Parse(download.text);
			if (int.Parse(tmpresp["resp"]) == 1){
				Application.LoadLevel("LogIn");
			}
			else{
				Debug.Log("Ya existe un usuario con ese nombre, pruebe con uno diferente");
			}
			Debug.Log("GCM registration ID set.");
		}
	}
}
