﻿using UnityEngine;
using System.Collections;

public class fadeManager : MonoBehaviour {

	public void fadeOut(){
		GetComponent<UISprite>().enabled = true;
		GetComponent<UITweener> ().PlayForward ();
	}

	public void fadeIn(){
		GetComponent<UISprite>().enabled = true;
		GetComponent<UITweener> ().PlayReverse ();
	}
}
