﻿using UnityEngine;
using System.Collections;

public class NextPrevButton : MonoBehaviour {
	public bool Positive = true;
	// Use this for initialization
	void Start()
	{
		UIButton button = GetComponent<UIButton>();
		EventDelegate.Set(button.onClick, delegate() { GameObject.Find("RankingManager").GetComponent<RankingManager>().NextGame(Positive); });
	}

}
