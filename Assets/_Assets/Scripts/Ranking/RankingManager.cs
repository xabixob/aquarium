﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//using Prime31;
using SimpleJSON;

public class RankingManager : MonoBehaviour {


	UILabel loading;
	public UILabel gameTitle;

	string[] GAME ={"Kill the Rubish","Pair and Win ("+Language.Get("EASY")+")","Pair and Win ("+Language.Get("MEDIUM")+")", "Pair and Win ("+Language.Get("HARD")+")", "Race the Whale ("+Language.Get("EASY")+")", "Race the Whale ("+Language.Get("MEDIUM")+")", "Race the Whale ("+Language.Get("HARD")+")"};
	int gameIndex = 0; 

	public void NextGame( bool positive = true)
	{
		int i = -1;
		if(positive)
			i = 1;
		gameIndex = ((gameIndex+i)%7 + 7)% 7;
		gameTitle.text = GAME[gameIndex];

		StartCoroutine(LoadRanking());
	}

	void Start()
	{
		gameTitle.text = GAME[gameIndex];
		StartCoroutine(LoadRanking());
	}
	IEnumerator LoadRanking () 
	{
	
		string post_url =  Constants.URL+"getRanking.php?idGame=" + (gameIndex+1);
		// Post the URL to the site and create a download object to get the result.
		
		WWW hs_post = new WWW(post_url);

		yield return hs_post; // Wait until the download is done
		
		if (hs_post.error != null)
		{   
			LoadNewWarningMessage("WARNIN_1");

		}else{
			JSONNode tmpresp = SimpleJSON.JSONArray.Parse(hs_post.text);

			for (int i =0;i<10;i++){
				if (tmpresp.Count>i){

					if (gameIndex==0){
						GameObject.Find("Rank"+(i+1)+"/puntos").GetComponent<UILabel>().text = tmpresp[i]["puntos"];
					}
					else{
						GameObject.Find("Rank"+(i+1)+"/puntos").GetComponent<UILabel>().text = (float)(tmpresp[i]["puntos"].AsInt/100f) + "";
					}
					GameObject.Find("Rank"+(i+1)+"/nombre").GetComponent<UILabel>().text = tmpresp[i]["nombre"];
				}
				else{
					GameObject.Find("Rank"+(i+1)+"/puntos").GetComponent<UILabel>().text = 0+"";
					GameObject.Find("Rank"+(i+1)+"/nombre").GetComponent<UILabel>().text = "-------";
				}
			}


		}

	}

	private void LoadNewWarningMessage(string mess){
		GameObject.Find ("UI Root").transform.FindChild ("ErrorMessage/Panel/Messaje").GetComponent<UILabel> ().text = Language.Get (mess);
		GameObject.Find ("UI Root").transform.FindChild ("ErrorMessage").gameObject.SetActive (true);
		GameObject.Find ("UI Root").transform.FindChild ("ErrorMessage/Panel").GetComponent<TweenScale> ().PlayForward ();
	}
}