﻿using UnityEngine;
using System.Collections;

public class MenuLanguageChange : MonoBehaviour {

	//SphereCollider sc;
	//float r;
	int languageIndex = 0;
	LanguageCode [] languages = {LanguageCode.EU, LanguageCode.ES, LanguageCode.EN, LanguageCode.FR};
	public UILabel text;
	UISprite sprite;
	void Awake()
	{
		/*TweenPosition tw = GetComponent<TweenPosition>();
		tw.from = transform.localPosition;
		tw.to = tw.from - new Vector3(3f, 8f,0f);*/
		sprite = GetComponent<UISprite>();
		SetLanguageToCurrent();
	}
	/*void Start () {
		sc = GetComponent<SphereCollider>();
		r = sc.radius;

	}*/
/*	void Update()
	{
		sc.radius = r * transform.localScale.x;
	}*/

	public void ChangeLanguage()
	{
		LanguageCode l = Language.CurrentLanguage();
		++languageIndex;

		int ll = (languageIndex%4);// + 3)% 3;
		languageIndex = ll;
		Language.SwitchLanguage(languages[languageIndex]);
		SetLanguageToCurrent();
		//Application.LoadLevel("menu");
		StartCoroutine(WaitAndChange(l));
	}

	IEnumerator WaitAndChange(LanguageCode l)
	{
		while( l == Language.CurrentLanguage())
		{
			yield return new WaitForSeconds(0.1f);
		}
		Application.LoadLevel("menu");
	}
	void SetLanguageToCurrent()
	{
		switch(Language.CurrentLanguage())
		{
		case LanguageCode.EU:
		{
			text.text = "EU";
			sprite.spriteName = "Euskera_Select";
			languageIndex = 0;
			break;
		}
		case LanguageCode.EN:
		{
			text.text = "EN";
			sprite.spriteName = "Ingles_Select";
			languageIndex = 2;
			break;
		}
		case LanguageCode.ES:
		{
			text.text = "ES";
			sprite.spriteName = "Spanish_Select";
			languageIndex = 1;
			break;
		}
		case LanguageCode.FR:
		{
			text.text = "FR";
			sprite.spriteName = "French_Select";
			languageIndex = 3;
			break;
		}
		}
	}
}
