﻿using UnityEngine;
using System.Collections;
using SimpleJSON;

public class UpdateMedals : MonoBehaviour {

	private AchievementManager am;
	private int currentTrio = 0;
	private ArrayList medallaOrder;
	private int lastMedal=0;
	public UISprite medFondo1,medFondo2,medFondo3,medCont1,medCont2,medCont3;
	public UILabel medLabel1, medLabel2, medLabel3;
	public GameObject containerMedallero;


	void Start(){
		am = GameObject.Find("AchievementsManager").GetComponent<AchievementManager>();

	}
	
	// Update is called once per frame
	public void UpdateMedallasList () {
		am = GameObject.Find("AchievementsManager").GetComponent<AchievementManager>();
		medallaOrder = new ArrayList();
		lastMedal = 0;
		for (int i=0; i<am.achivementsArray.Count; i++) {
			if (am.achivementsArray[i].AsInt>0){
				medallaOrder.Add(i);
				lastMedal++;
			}
		}
		for (int i=0; i<am.achivementsArray.Count; i++) {
			if (am.achivementsArray[i].AsInt<0){
				medallaOrder.Add(i);
			}
		}

		StartCoroutine("ReloadNextMedallas");
	}

	//Modificar periodicamente las medallas
	IEnumerator ReloadNextMedallas(){
		// Seleccionamos que vamos a mostrar, solo hay una medalla en la ultima muestra
		while(true){
			if (currentTrio < 14) {
				medFondo3.transform.parent.gameObject.SetActive (true);
				int tmpval;
				medFondo1.spriteName = Constants.BACKGROUND_MEDALS [(int)medallaOrder[3 * currentTrio]];
				medFondo2.spriteName = Constants.BACKGROUND_MEDALS [(int)medallaOrder[3 * currentTrio +1]];
				medFondo3.spriteName = Constants.BACKGROUND_MEDALS [(int)medallaOrder[3 * currentTrio +2]];
				medFondo1.GetComponent<OnClickMedalla>().medallanumber = (int)medallaOrder[3 * currentTrio];
				medFondo2.GetComponent<OnClickMedalla>().medallanumber = (int)medallaOrder[3 * currentTrio+1];
				medFondo3.GetComponent<OnClickMedalla>().medallanumber = (int)medallaOrder[3 * currentTrio+2];

				if(int.TryParse(Constants.CONTENIDO_MEDALS [(int)medallaOrder[3 * currentTrio]],out tmpval)){
					medCont1.gameObject.SetActive(false);
					medLabel1.gameObject.SetActive(true);
					medLabel1.text = int.Parse(Constants.CONTENIDO_MEDALS [(int)medallaOrder[3 * currentTrio]])+"";
				}
				else{
					medCont1.gameObject.SetActive(true);
					medLabel1.gameObject.SetActive(false);
					medCont1.spriteName = Constants.CONTENIDO_MEDALS [(int)medallaOrder[3 * currentTrio]];
				}
				if(int.TryParse(Constants.CONTENIDO_MEDALS [(int)medallaOrder[3 * currentTrio+1]],out tmpval)){
					medCont2.gameObject.SetActive(false);
					medLabel2.gameObject.SetActive(true);
					medLabel2.text = int.Parse(Constants.CONTENIDO_MEDALS [(int)medallaOrder[3 * currentTrio+1]])+"";
				}
				else{
					medCont2.gameObject.SetActive(true);
					medLabel2.gameObject.SetActive(false);
					medCont2.spriteName = Constants.CONTENIDO_MEDALS [(int)medallaOrder[3 * currentTrio+1]];
				}
				if(int.TryParse(Constants.CONTENIDO_MEDALS [(int)medallaOrder[3 * currentTrio+2]],out tmpval)){
					medCont3.gameObject.SetActive(false);
					medLabel3.gameObject.SetActive(true);
					medLabel3.text = int.Parse(Constants.CONTENIDO_MEDALS [(int)medallaOrder[3 * currentTrio+2]])+"";
				}
				else{
					medCont3.gameObject.SetActive(true);
					medLabel3.gameObject.SetActive(false);
					medCont3.spriteName = Constants.CONTENIDO_MEDALS [(int)medallaOrder[3 * currentTrio+2]];
				}


				if (lastMedal<=(3 * currentTrio)){
					medFondo1.color = new Color(1,1,1,0.3f);
					medCont1.color = new Color(1,1,1,0.3f);
					medLabel1.color = new Color(1,1,1,0.3f);
				}
				else{
					medFondo1.color = new Color(1,1,1,1);
					medCont1.color = new Color(1,1,1,1);
					medLabel1.color = new Color(1,1,1,1);
				}
				if (lastMedal<=(3 * currentTrio+1)){
					medFondo2.color = new Color(1,1,1,0.3f);
					medCont2.color = new Color(1,1,1,0.3f);
					medLabel2.color = new Color(1,1,1,0.3f);
				}
				else{
					medFondo2.color = new Color(1,1,1,1);
					medCont2.color = new Color(1,1,1,1);
					medLabel2.color = new Color(1,1,1,1);
				}
				if (lastMedal<=(3 * currentTrio+2)){
					medFondo3.color = new Color(1,1,1,0.3f);
					medCont3.color = new Color(1,1,1,0.3f);
					medLabel3.color = new Color(1,1,1,0.3f);
				}
				else{
					medFondo3.color = new Color(1,1,1,1);
					medCont3.color = new Color(1,1,1,1);
					medLabel3.color = new Color(1,1,1,1);
				}
			} else {
				int tmpval;
				medFondo3.transform.parent.gameObject.SetActive (false);
				medFondo1.spriteName = Constants.BACKGROUND_MEDALS [(int)medallaOrder[3 * currentTrio]];
				medFondo2.spriteName = Constants.BACKGROUND_MEDALS [(int)medallaOrder[3 * currentTrio +1]];
				medFondo1.GetComponent<OnClickMedalla>().medallanumber = (int)medallaOrder[3 * currentTrio];
				medFondo2.GetComponent<OnClickMedalla>().medallanumber = (int)medallaOrder[3 * currentTrio+1];
				if(int.TryParse(Constants.CONTENIDO_MEDALS [(int)medallaOrder[3 * currentTrio]],out tmpval)){
					medCont1.gameObject.SetActive(false);
					medLabel1.gameObject.SetActive(true);
					medLabel1.text = int.Parse(Constants.CONTENIDO_MEDALS [(int)medallaOrder[3 * currentTrio]])+"";
				}
				else{
					medCont1.gameObject.SetActive(true);
					medLabel1.gameObject.SetActive(false);
					medCont1.spriteName = Constants.CONTENIDO_MEDALS [(int)medallaOrder[3 * currentTrio]];
				}
				if(int.TryParse(Constants.CONTENIDO_MEDALS [(int)medallaOrder[3 * currentTrio+1]],out tmpval)){
					medCont2.gameObject.SetActive(false);
					medLabel2.gameObject.SetActive(true);
					medLabel2.text = int.Parse(Constants.CONTENIDO_MEDALS [(int)medallaOrder[3 * currentTrio+1]])+"";
				}
				else{
					medCont2.gameObject.SetActive(true);
					medLabel2.gameObject.SetActive(false);
					medCont2.spriteName = Constants.CONTENIDO_MEDALS [(int)medallaOrder[3 * currentTrio+1]];
				}

				if (lastMedal<=(3 * currentTrio)){
					medFondo1.color = new Color(1,1,1,0.3f);
					medCont1.color = new Color(1,1,1,0.3f);
					medLabel1.color = new Color(1,1,1,0.3f);
				}
				else{
					medFondo1.color = new Color(1,1,1,1);
					medCont1.color = new Color(1,1,1,1);
					medLabel1.color = new Color(1,1,1,1);
				}
				if (lastMedal<=(3 * currentTrio+1)){
					medFondo2.color = new Color(1,1,1,0.3f);
					medCont2.color = new Color(1,1,1,0.3f);
					medLabel2.color = new Color(1,1,1,0.3f);
				}
				else{
					medFondo2.color = new Color(1,1,1,1);
					medCont2.color = new Color(1,1,1,1);
					medLabel2.color = new Color(1,1,1,1);
				}
			}

			containerMedallero.GetComponent<TweenColor> ().from = new Color (1, 1, 1, 0);
			containerMedallero.GetComponent<TweenColor> ().to = new Color (1, 1, 1, 1);
			containerMedallero.GetComponent<TweenColor> ().ResetToBeginning();
			containerMedallero.GetComponent<TweenColor> ().enabled = true;
			yield return new WaitForSeconds (5f);
			containerMedallero.GetComponent<TweenColor> ().from = new Color (1, 1, 1, 1);
			containerMedallero.GetComponent<TweenColor> ().to = new Color (1, 1, 1, 0);
			containerMedallero.GetComponent<TweenColor> ().ResetToBeginning();
			containerMedallero.GetComponent<TweenColor> ().enabled = true;

			yield return new WaitForSeconds (1f);
			currentTrio++;
			if (currentTrio == 15)
				currentTrio = 0;
		}
	}
}
