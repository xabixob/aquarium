﻿using UnityEngine;
using System.Collections;

public class OnCloseWarningMessageClick : MonoBehaviour {

	// Update is called once per frame
	void OnClick () {
		transform.parent.parent.localScale = new Vector3 (0, 0, 0);
		transform.parent.parent.parent.gameObject.SetActive (false);
	}
}
