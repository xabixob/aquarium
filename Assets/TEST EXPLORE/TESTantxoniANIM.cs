﻿using UnityEngine;
using System.Collections;

public class TESTantxoniANIM : MonoBehaviour {

	UISpriteAnimation anim;
	// Use this for initialization
	
	void Start () {
		anim = GetComponentInChildren<UISpriteAnimation> ();
		anim.namePrefix="Idle_";
		anim.Play();
	}
	
	// Update is called once per frame
	void Update () {
		
		if (!anim.isPlaying) {
			anim.namePrefix="Idle_";
			anim.loop=true;
			anim.Play();
		}
	}
}
