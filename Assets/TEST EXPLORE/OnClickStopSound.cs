﻿using UnityEngine;
using System.Collections;

public class OnClickStopSound : MonoBehaviour {

	public GameObject soundContainers;

	// Use this for initialization
	void OnClick () {
		AudioSource[] audioComponents = soundContainers.GetComponentsInChildren<AudioSource>(true);
		foreach(AudioSource component in audioComponents)
		{
			component.Stop();
		}
		gameObject.SetActive (false);
	}
	

}
